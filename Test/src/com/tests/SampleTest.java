/**
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 *																																		   	*
 * 2011-2012 Infosys Limited, Banglore, India. All Rights Reserved																			*
 * Version: 2.0																																*
 * 																																			*
 * Except for any free or open source software components embedded in this Infosys proprietary software program ("Program"),				*
 * this Program is protected by copyright laws, international treaties and other pending or existing intellectual property rights in India, *
 * the United States and other countries. Except as expressly permitted, any unautorized reproduction, storage, transmission 				*
 * in any form or by any means (including without limitation electronic, mechanical, printing, photocopying, recording or otherwise), 		*
 * or any distribution of this Program, or any portion of it, may result in severe civil and criminal penalties, 							*
 * and will be prosecuted to the maximum extent possible under the law 																		*
 *																																			*
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 **/
package com.tests;

import org.testng.annotations.Test;

import com.components.entities.Start;
import com.components.yaml.SearchData;
import com.iwaf.framework.Initiator;
import com.components.yaml.LoginData;

public class SampleTest extends Initiator{

//  //------------------------------------------------------Home Page--------------------------------------------------------------------	
	@Test(groups={"HomePage"}, description = "Verify Footer link",testName="Kohler->HomePage:31-34")
	public void verifyHomePageFooterLinks()
	{
		
		Start.asTester
			.goToHomepage().atHomePage().VerifyFooterLinks();	
		}
//	
//	@Test(priority = 1,groups={"HomePage1"}, description = "Verify utility bar and links",testName="Kohler->HomePage:1-2")
//	public void verifyUtilityLinks()
//	{
//		
//		Start.asTester
//			.goToHomepage().atHomePage().VerifyUtilitybarLinks();
//	}
//	
//	@Test(groups={"HomePage1"}, description = "Verify help us to improve more link and functionality",testName="Kohler->HomePage:29-30")
//	public void VerifyHelpUsToImproveMoreLink()
//	{
//		
//		Start.asTester
//			.goToHomepage().atHomePage().VerifyHelpUsToImproveMore("Test", "Suggestion");
//	}
//	
//	@Test(groups={"HomePage"}, description = "Verify Home page title",testName="Kohler->HomePage:6")
//	public void VerifyHomePageTitle()
//	{
//				Start.asTester
//			.goToHomepage().atHomePage().verifyHomePageTitle();
//	}
//	
//	@Test(groups={"HomePage"}, description = "Verify Home Page URL",testName="Kohler->HomePage:7")
//	public void VerifyHomePageURL()
//	{
//		
//		Start.asTester
//			.goToHomepage().atHomePage().verifyHomePageURL();
//	}
//	
//	@Test(groups={"HomePage"}, description = "Verify World wide expansion and countries",testName="Kohler->HomePage:3")
//	public void VerifyWorldWideCountries()
//	{
//		
//		Start.asTester
//			.goToHomepage().atHomePage().VerifyWorldWideCountriesLink();
//	}
//		
//	@Test(groups={"HomePage"}, description = "Verify Brand tray visibility on scroll down",testName="Kohler->HomePage:5")
//	public void VerifyBrandTrayVisibilityOnScrollDown()
//	{
//		
//		Start.asTester
//			.goToHomepage().atHomePage().VerifyBrandTrayVisibility();
//	}
//	
//	
//	@Test(groups={"HomePage"}, description = "Verify Hero and nav dots functionality",testName="Kohler->HomePage:8-11")
//	public void VerifyHerocarousel()
//	{
//		
//		Start.asTester
//			.goToHomepage().atHomePage().VerifyHero();
//	}
//	
//	@Test(groups={"HomePage"}, description = "Verify Links other then kohler opens in new window",testName="Kohler->HomePage: 4")
//	public void VerifylinksOtherToKohlerBrand()
//	{
//		
//		Start.asTester
//			.goToHomepage().atHomePage().VerifyLinkOtherKohlerBrands();
//	}
//	
//	
//	@Test(description = "Home Page - KCC (#KohlerIdeas)", testName = "Kohler Home Page-22", groups={"HomePage"})
//    public void VerifyPromoImagesCount()
//    {
//           Start.asTester.goToHomepage().VerifyKohlerIdeaslayout_PromoImages();           
//    }
//    
//    @Test(description = "Home Page", testName = "Kohler Home Page-23", groups={"HomePage"})
//    public void VerifyArrows()
//    {
//           Start.asTester.goToHomepage().VerifyKohlerIdeaslayout_Arrows();         
//    }
//   
//    @Test(description = "Home Page" , testName = "Kohler Home Page-22", groups={"HomePage"})
//    public void VerifyButtons()
//    {
//           Start.asTester.goToHomepage().VerifyKohlerIdeaslayout_ButonsAndLinks();        
//    }
//    
//    @Test(description = "Home Page" , testName = "Kohler Home Page-24", groups={"HomePage"})
//    public void VerifyTootip()
//    {
//           Start.asTester.goToHomepage().VerifyKohlerIdeaslayout_ToolTip();        
//    }
//    
//    @Test(description = "Home Page", testName = "Kohler Home Page-26", groups={"HomePage"})
//    public void VerifyViewGallery()
//    {
//           Start.asTester.goToHomepage().VerifyViewGalleryScreen();   
//    }
//    
//    @Test(description = "Home Page", testName = "Kohler Home Page-25", groups={"HomePage"})
//    public void VerifyOverlayDisplayPopup()
//    {
//           Start.asTester.goToHomepage().VerifyKohlerIdeas_OverlayDisplay();	
//           
//    }
//    
//    @Test(description = "Home Page - Promo Grid", testName = "Kohler Home Page-13,14,15", groups={"HomePage"})
//    public void VerifyPromoModuleGid() throws InterruptedException
//    {
//           Start.asTester.goToHomepage().VerifyPromoModuleGrid();            
//    }
//   
//    @Test(description = "Home Page - Discover the Possibilities", testName = "Kohler Home Page-16", groups={"HomePage"})
//    public void VerifyDiscoverthePossibilities_NavCircles()
//    {
//           Start.asTester.goToHomepage().VerifyDiscoverthePossibilities_NavCircles();            
//    }
//    
//    @Test(description = "Home Page - Discover the Possibilities", testName = "Kohler Home Page-16", groups={"HomePage"})
//    public void VerifyDiscoverthePossibilities_Arrows()
//    {
//           Start.asTester.goToHomepage().VerifyDiscoverthePossibilities_Arrows();   	      
//    }
//    
//    @Test(description = "Home Page - Discover the Possibilities", testName = "Kohler Home Page-17 part 1", groups={"HomePage"})
//    public void VerifyDiscoverthePossibilities_HotSpots() throws InterruptedException
//    {
//           Start.asTester.goToHomepage().VerifyDiscoverthePossibilities_HotSpots_GetDetails();              
//    }
//    
//    @Test(description = "Home Page - Discover the Possibilities", testName = "Kohler Home Page-17 part 2", groups={"HomePage"})
//    public void VerifyDiscoverthePossibilities_HotSpots1() throws InterruptedException
//    {
//           Start.asTester.goToHomepage().VerifyDiscoverthePossibilities_HotSpots_StoreLocator();       	 
//    }
//    
//    @Test(description = "Home Page - Discover the Possibilities", testName = "Kohler Home Page-18", groups={"HomePage"})
//    public void VerifyDiscoverthePossibilities_Learnmore()
//    {
//           Start.asTester.goToHomepage().VerifyDiscoverthePossibilities_LearnMore();             
//    }
//    
//    @Test(description = "Home-Page", testName = "My Acc-Folder-16 and Home-Page -20", groups={"HomePage"})   
//    public void VerifyAddtoFolderSignin() throws InterruptedException
//    {
//    Start.asTester.goToHomepage().signIn("LoginData").VerifyDiscoverthePossibilities_AddToFolderSignin();              
//    }
//    
//    @Test(description = "Home Page - Discover the Possibilities", testName = "Kohler Home Page-21", groups={"HomePage"})
//    public void VerifyDiscoverthePossibilities_Share()
//    {
//           Start.asTester.goToHomepage()
//           .VerifyDiscoverthePossibilities_Share();          
//    }
//
//  //------------------------------------------------------My Account Folder--------------------------------------------------------------------
//    @Test(description = "Home Page", testName = "My Acc-Folder-1,2 and Home-Page -19", groups= {"My Acc-Folder"})
//    public void VerifyDiscoverthePossibilities_AddtoFolder() throws InterruptedException
//    {
//           Start.asTester.goToHomepage().VerifyDiscoverthePossibilities_AddToFolder();           
//    }
//    
//    @Test(description = "Verifying Edit and delete action items from my folder for Not signed in", testName = "My Acc-Folder-4", groups= {"My Acc-Folder"})
//    public void VerifyEdit_Delete_NotSignedIn() throws InterruptedException
//    {
//           Start.asTester.goToHomepage().VerifyEditDelete_NotSignedIn();            
//    }
//   
//    @Test(description = "Verifying Sign in", testName = "My Acc-Folder-5,8,9,10", groups= {"My Acc-Folder"})
//    public void VerifySignIn()
//    {
//           Start.asTester.goToHomepage().VerifySignIn();        
//    }
//    
//    @Test(description = "Verifying Sign out", testName = "My Acc-Folder-7", groups= {"My Acc-Folder"})
//    public void VerifySignOut() throws InterruptedException
//    {
//           Start.asTester.goToHomepage().signIn("LoginData").VerifySignout();             
//    }
//      
//    @Test(description = "Verify Account edit", testName = "My Acc-Folder-11", groups= {"My Acc-Folder"})
//    public void VerifyAccountEdit()
//    {
//           Start.asTester.goToHomepage().signIn("LoginData").VerifyAccountEdit();
//    }
//    
//    @Test(description = "Verify creating new folder and access of new folder", testName = "My Acc-Folder-14,15", groups= {"My Acc-Folder"})
//    public void VerifyAddNewFolder() throws InterruptedException
//    {
//           Start.asTester.goToHomepage().signIn("LoginData").VerifyAddNewFolder("SearchDataFolder");              
//    }
//    
//    @Test(description = "Verify Copy Action from My folder", testName = "My Acc-Folder-17 - Copy", groups= {"My Acc-Folder"})
//    public void VerifyFoldepPageOptions() throws InterruptedException
//    {
//    Start.asTester.goToHomepage().signIn("LoginData").VerifyMyFolderPageOptions("SearchDataFolderNew");   
//    }
//    
//    @Test(description = "Verify Copy Action from My folder", testName = "My Acc-Folder-18 - Copy", groups= {"My Acc-Folder"})  
//    public void VerifyCopy_MyFolder() throws InterruptedException
//    {
//    Start.asTester.goToHomepage().signIn("LoginData").VerifyMyFoldersCopyAction_SignedIn("SearchDataFolder");                          
//    }        
//    
//    @Test(description = "Verify Move Action from My folder", testName = "My Acc-Folder-18 - Move", groups= {"My Acc-Folder"})   
//    public void VerifyMove_MyFolder() throws InterruptedException
//    {
//    Start.asTester.goToHomepage().signIn("LoginData").VerifyMyFoldersMoveAction_SignedIn("SearchDataFolder");            
//    }
//   
//   @Test(description = "Verifying Edit and delete action from my folder items for signed in", testName = "My Acc-Folder-18", groups= {"My Acc-Folder"})
//    public void VerifyEdit_Delete_SignedIn() throws InterruptedException
//    {
//    Start.asTester.goToHomepage().signIn("LoginData").VerifyEditDelete_SignedIn("SearchDataFolder");            
//    }
//	
//	/*------------------------General Navigation Test cases-----------------------------------*/
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->Bathroom", testName= "Kohler ->Gen Nav:1-5")
//	public void VerifyGenNavBathroom()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyBathroomMainMenu();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->Bathroom->Sub menu links", testName= "Kohler ->Gen Nav: 6")
//	public void VerifyGenNavBathroomProducts()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.verifyBathroomSubMenuLinks();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->choreographShowerPlanner link", testName= "Kohler ->Gen Nav: 7")
//	public void VerifyGenNavChoreographShowerPlanner()
//	{
//		
//		Start.asTester  
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.verifychoreographShowerPlanner();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->Bathroom->ProductBuyingGuide link/Share Functioanlity", testName= "Kohler ->Gen Nav: 8,9")
//	public void VerifyGenNavBathroomProductBuyingGuide()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyBathroomProductBuyingGuide();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->Kitchen", testName= "Kohler ->Gen Nav: 10,11")
//	public void VerifyGenNavKitchen()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyKitchenMainMenu();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->Kitchen->Sub menu links", testName= "Kohler ->Gen Nav: 12,14")
//	public void VerifyGenNavKitchenProducts()
//	{
//		
//		Start.asTester
//			.goToHomepage()._GoToGeneralNavigation()
//			.verifyKitchenSubMenuLinks();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify Footer->Press Room", testName= "Kohler ->Gen Nav: 29,30")
//	public void VerifyFooterPressRoom()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()             
//			.VerifyPressRoomLink();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General navigation->Find a store", testName= "Kohler ->Gen Nav: 31,32")
//	public void VerifyGenNavFindaStore()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyFindAStore();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General navigation->Kitchen->Planner", testName= "Kohler ->Gen Nav: 15")
//	public void VerifyGenNavKitchenPlanner()
//	{
//		
//		Start.asTester
//			.goToHomepage() 
//			._GoToGeneralNavigation()
//			.verifyKitchenPlanner();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->Kitchen->ProductBuyingGuide link/Share Functioanlity", testName= "Kohler ->Gen Nav: 16,17")
//	public void VerifyGenNavKitchenProductBuyingGuide()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyKitchenProductBuyingGuide();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->ideas", testName= "Kohler ->Gen Nav: 24")   
//	public void VerifyGenNavIdeas()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyIdeasMainMenu();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->ideas->Kitchen Planner", testName= "Kohler ->Gen Nav: 26")
//	public void VerifyGenNavIdeasKitchenPlanner()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.verifyIdeasKitchenPlanner();
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->ideas->Sub menu Links", testName= "Kohler ->Gen Nav: 25,27")
//	public void VerifyGenNavIdeasSubMenuLinks()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.verifyIdeasSubMenuLinks();
//	}
//
//	@Test(groups={"GenNavigation"}, description = "Verify General Navigation->Kitchen->HelpUsToImprove Link", testName= "Kohler ->Gen Nav: 40")
//	public void VerifyGenNavKitchenHelpUsToImprove()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyKitchenHelpUsToImproveMore("Test", "Dislike");
//	}
//
//	@Test(groups={"GenNavigation"}, description = "Verify NewsLetter Sign Up Links/functioanlity", testName= "Kohler->Gen Nav: 41")  //-----------issue
//	public void VerifyNewLetterSignUpLinks()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyNewsLetterSignUpLink("SearchData");
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify Parts Wizard functioanlity", testName= "Kohler->Gen Nav: 18-23")
//	public void VerifyPartsWizard()
//	{
//	
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyPartsMainMenu();
//	}
//
//	@Test(groups={"GenNavigation"}, description = "Verify Find a Pro Functioanlity with invalid Zip code", testName= "Kohler->Gen Nav: 33,34")  
//	public void VerifyFindAProInvalidZipCode()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyFindAProInvalidZipCode("SearchDataWithNull");
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify Find a Pro Functioanlity with valid Zip code", testName= "Kohler->Gen Nav: 35,36") 
//	public void VerifyFindAProValidZipCode()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.VerifyFindAPro("SearchData1","SearchData2");
//	}
//	
//	@Test(groups={"GenNavigation"}, description = "Verify Page Title End With KOHLER", testName= "Kohler->Gen Nav: 42")
//	public void VerifyPageTitleEndWithKOHLER()
//	{
//		
//		Start.asTester
//			.goToHomepage()
//			._GoToGeneralNavigation()
//			.verifyKitchenSubMenuLinksPageTitles();
//	}
//	//---------------------------------------------------------Kohler PDP Page------------------------------------------------------------------------------
//	
//  @Test(description = "PDP Page", testName = "Kohler PDP Page-6", groups="KohlerPDPPage")
//  public void VerifyPDPPage()
//  {
//         Start.asTester.goToKohlerproductdisplaypage().VerifyPDPpage("SearchData");
//  }
//  
//  
//  @Test(description = "PDP Page",testName = "Kohler PDP Page-9,10", groups="KohlerPDPPage")
//  public void VerifyPDPPage_Breadcrumbs_StoreLocator()
//  {
//  Start.asTester.goToKohlerproductdisplaypage().VerifyBreadcrumbs_StoreLocator_PDPpage("SearchDataSKu");    
//  }
//  
//  @Test(description = "Verify compare overlay displays and Dissapear",testName = "Kohler PDP Page - 12,14", groups="KohlerPDPPage")
//  public void VerifyPDPPage_CompareOverlayDisplay() throws InterruptedException
//  {
//  Start.asTester.goToKohlerproductdisplaypage().VerifyCompareOverlayDisplayandDisappear("SearchDataSKu");
//  												
//  }
//  
//  @Test(description = "Verify Compare Modal",testName = "Kohler PDP Page - 13", groups="KohlerPDPPage")   
//  public void VerifyCompareModal() throws InterruptedException
//  {
//         Start.asTester.goToKohlerproductdisplaypage().VerifyCompareproductModal();
//  }
//  
//  @Test(description = "Verify Add To Cart",testName = "Kohler PDP Page - 15,16,17", groups="KohlerPDPPage")
//  public void VerifyPDPPage_AddToCart() throws InterruptedException
//  {
//         Start.asTester.goToKohlerproductdisplaypage().VerifyAddtoCart_PDPPage("SearchDataSKu");
//  }
//  
//  @Test(description = "Verify Search and comapare product after signed In",testName = "Kohler PDP Page - 18,19", groups="KohlerPDPPage")
//  public void VerifyPDPPage_Compare_Signin() throws InterruptedException
//  {
//  Start.asTester.goToKohlerproductdisplaypage().signIn("LoginData").VerifyCompareOverlayDisplayandDisappear("SearchDataSKu");
//  }
//  
//  @Test(description = "Verify Search and Add product to cart after signed In",testName = "Kohler PDP Page - 20", groups="KohlerPDPPage")
//  public void VerifyPDPPage_AddToCart_Signin() throws InterruptedException
//  {
//  Start.asTester.goToKohlerproductdisplaypage().signIn("LoginData").VerifyAddtoCart_PDPPage("SearchDataSKu");
//  }
//  
//  @Test(description = "Verify Options PDP page",testName = "Kohler PDP Page - 25,26", groups="KohlerPDPPage")
//  public void VerifyPDPPage_Options() throws InterruptedException
//  {
//         Start.asTester.goToKohlerproductdisplaypage().VerifyOptions_PDPPage("SearchDataSKu1");
//  }
//  
//  @Test(description = "Verify Discontinued PDP page",testName = "Kohler PDP Page - 29", groups="KohlerPDPPage")
//  public void VerifyPDPPage_DiscontinuedProduct() throws InterruptedException
//  {
//         Start.asTester.goToKohlerproductdisplaypage().DiscontinuedProduct("SearchDataSKu2");
//  }
//  
// 
//  @Test(description = "Verify SellableServicepart added to cart",testName = "Kohler PDP Page - 21", groups="KohlerPDPPage")
//  public void VerifyPDPPage_SellableServiceParts() throws InterruptedException
//  {
//         Start.asTester.goToKohlerproductdisplaypage().SellableServicepart("SearchDataSKu4");
//  }
//
//	
///*------------------------------DTV Page------------------------------------------------------------*/
//	  	
//	@Test(groups={"DTV"}, description = "Verify DTV Page Load", testName = "Kohler_DTV+BEP-1")
//	public void verifyDTVPageLoad() 
//	{	
//		Start.asTester
//		.goToDTVPage()
//		.verifyDTVPageLoad();			
//	}
//	
//	@Test(groups={"DTV"}, description = "Verify DTV Global Nav", testName = "Kohler_DTV+BEP-2")
//	public void verifyDTV_Global_Navigation() 
//	{	
//		Start.asTester
//		.goToDTVPage()
//		.verifyDTVGlobalNav();			
//	}
//	
//	@Test(groups={"DTV"}, description = "Verify DTV Page Title", testName = "Kohler_DTV+BEP-3")
//	public void verifyDTVPageTitle() 
//	{	
//		Start.asTester
//		.goToDTVPage()
//		.verifyDTVTitle();			
//	}
//	
//	@Test(groups={"DTV"}, description = "Verify DTV Hero Nav", testName = "Kohler_DTV+BEP-4")
//	public void verifyDTV_Hero_Navigation() 
//	{	
//		Start.asTester
//		.goToDTVPage()
//		.verifyDTVHeroNav();			
//	}
//	
//	@Test(groups={"DTV"}, description = "Verify Shower Experience", testName = "Kohler_DTV+BEP-5") 
//	public void verifyDTV_Shower_Experience() 
//	{	
//		Start.asTester
//		.goToDTVPage()
//		.verifyDTVShowerExperience();			
//	}
//		
//	@Test(groups={"DTV"}, description = "Verify Spa Experience", testName = "Kohler_DTV+BEP-6")
//	public void verifyDTV_Spa_Experience() 
//	{	
//		Start.asTester
//		.goToDTVPage()
//		.verifyDTVSpaExperience();			
//	}
//	
	@Test(groups={"DTV"}, description = "Verify DTV Page Load", testName = "Kohler_DTV+BEP-7") //send keys input	---fail
	public void verifyDTV_WTB_Section() 
	{	
		Start.asTester
		.goToDTVPage()
		.verifyDTVWTB();			
	}
//	
//	@Test(groups={"DTV"}, description = "Verify DTV Share Section", testName = "Kohler_DTV+BEP-8")
//	public void verifyDTVShareSection() 
//	{	
//		Start.asTester
//		.goToDTVPage()
//		.verifyDTVShare();			
//	}
//	/*-----------------------------------------------Kohler Search Page---------------------------------------------*/
//	
//	@Test(groups={"Search"}, description = "Verify access to Kitchen section landing page " ,testName ="Kohler->Search:1")
//    public void VerifySearchFunctionalityKitchen()
//    {
//        Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityKitchen();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify access to Toilets category landing page " ,testName ="Kohler->Search:2")
//    public void VerifySearchFunctionalityToilets()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityToilets();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify access to Vanity category landing page " ,testName ="Kohler->Search:3")
//    public void VerifySearchFunctionalityVanity()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityVanity();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify access to Faucet category landing page " ,testName ="Kohler->Search:4")
//    public void VerifySearchFunctionalityFaucet()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityFaucet();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify access to Product detail page " ,testName ="Kohler->Search:5")
//    public void VerifySearchFunctionalityProduct()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityProduct();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify search for keyword Arm " ,testName ="Kohler->Search:6")
//    public void VerifySearchFunctionalityArm()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityArm();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify Inspiration tabs " ,testName ="Kohler->Search:7")
//    public void VerifySearchFunctionalityInspirationResource()
//    {
//		Start.asTester.goToKolherSearchPage().VerifyFunctionalityInspiration().VerifyFunctionalityResource();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify search and collection is displayed " ,testName ="Kohler->Search:9")
//    public void VerifySearchFunctionalityMemoirs()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityMemoirs();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify search and moxie aricle is displayed " ,testName ="Kohler->Search:10")
//    public void VerifySearchFunctionalityMoxie()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityMoxie();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify search and results for 'did you mean two? " ,testName ="Kohler->Search:11")
//    public void VerifySearchFunctionalityTw()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityTw();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify typeahead with 3 letters " ,testName ="Kohler->Search:12")
//    public void VerifySearchFunctionalityTou()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityTou();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify search and page loads to external website " ,testName ="Kohler->Search:13")
//    public void VerifySearchFunctionalityToLoadExternalWebSite()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityTerm_Leed().VerifySearchFunctionalityTerm_Robern().VerifySearchFunctionalityTerm_Privacy();
//    }
//	
//	@Test(groups={"Search"}, description = "Verify search and page loads to internal website " ,testName ="Kohler->Search:14")
//    public void VerifySearchFunctionalityFunctionalityToLoadInternalWebSite()
//    {
//		Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityTerm_BathTub().VerifySearchFunctionalityTerm_NightLight().VerifySearchFunctionalityTerm_PressureBalance();
//    }  
//	
//  @Test(groups={"Search"}, description = " Verify each term display the same results" ,testName ="Kohler->Search:15")	
//	public void VerifySearchFunctionalityTermsDisplaySameResults()
//	{
//    	Start.asTester.goToKolherSearchPage().VerifySearchFunctionalityTermsDisplaySameResults();
//	}
//	
//	@Test(groups={"Search"}, description = "Verify Articles are turned off by Parts" ,testName ="Kohler->Search:16")
//	public void VerifyArticlesTurnedOffbyParts()
//	{
//		Start.asTester.goToKolherSearchPage().VerifyArticlesTurnedOffbyParts();
//	}
//	
//	@Test(groups={"Search"}, description = "Verify Collections are turned off by Parts" ,testName ="Kohler->Search:17")
//	public void VerifyCollectionsTurnedOffbyParts()
//	{
//		Start.asTester.goToKolherSearchPage().VerifyCollectionsTurnedOffbyParts();
//	}
//	


}