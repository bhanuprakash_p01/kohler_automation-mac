package com.components.pages;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.components.repository.SiteRepository;
import com.components.yaml.NewsLetterSignUp;
import com.components.yaml.SearchData;
import com.iwaf.framework.components.Target;
import com.iwaf.framework.components.IReporter.LogType;
import com.sun.corba.se.impl.ior.GenericTaggedComponent;

public class Kohler_GeneralNavigation extends SitePage
{
	
	/* Defining the locators on the Page */ 
	
	public static final Target Link_bathroom = new Target("Link_bathroom","//button[@id= 'main-nav__button--bathroom']",Target.XPATH);	
	public static final Target Link_bathroomMainMenu = new Target("Link_bathroommainMenu","//div[@id='main-nav']//following::div[@id='sub-nav-tab--bathroom']",Target.XPATH);	
	public static final Target Link_bathroomPage = new Target("Link_bathroomPage","//*[@id='sub-nav-tab--bathroom']//following::a[text()=' Bathroom']",Target.XPATH);	
	public static final Target Link_bathroomPage_sink = new Target("Link_bathroomPage_sink","//*[@id='sub-nav-tab--bathroom']//following::a[text()=' Bathroom']",Target.XPATH);
	public static final Target Link_ChoregraphShowerPlanner = new Target("Link_ChoregraphShowerPlanner","//*[@id='sub-nav-tab--bathroom']/div/div/div[3]/ul/li[6]/a",Target.XPATH);
	public static final Target Link_BathroomProductBuyingGuide = new Target("Link_BathroomProductBuyingGuide","//*[@id='sub-nav-tab--bathroom']/div/div/div[3]/ul/li[1]/a",Target.XPATH);
	public static final Target Link_KitchenProductBuyingGuide = new Target("Link_KitchenProductBuyingGuide","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[3]/ul/li[1]/a",Target.XPATH);
	public static final Target Header_SaveToFolder = new Target("Header_SaveToFolder","//*[@id='hideAddModal']/h1",Target.XPATH);
	public static final Target button_SaveToFolderClose = new Target("button_SaveToFolderClose","//*[@id='hideAddModal']/button",Target.XPATH);
	public static final Target textBox_SaveToFolder = new Target("textBox_SaveToFolder","//*[@id='textarea-140-5']",Target.XPATH);
	public static final Target button_SaveToFolderSave = new Target("button_SaveToFolderSave","//*[@id='addToFolder']/button[1]",Target.XPATH);
	public static final Target button_SaveToFolderCancel = new Target("button_SaveToFolderCancel","//*[@id='addToFolder']/button[2]",Target.XPATH);
	public static final Target title_ItemAddedToFold1= new Target("title_ItemAddedToFold1","//*[@id='showThankyou']/p/span[1]",Target.XPATH);
	public static final Target title_ItemAddedToFold2 = new Target("title_ItemAddedToFold2","//*[@id='showThankyou']/p/span[2]",Target.XPATH);
	public static final Target button_ItemAddedToFoldClose = new Target("button_ItemAddedToFoldClose","//*[@id='showThankyou']/button",Target.XPATH);
	public static final Target Items_shareItems = new Target("Items_shareItems","//*[@class='share-tip organic-namespace olbox-container']",Target.XPATH);
	public static final Target Items_shareItemFaceebook = new Target("Items_shareItems","//*[@class='share-tip organic-namespace olbox-container']/div/ul/li[3]/a",Target.XPATH);
	public static final Target Win_ShowThankYou = new Target("Win_ShowThankYou","//*[@id='showThankyou']",Target.XPATH);
	public static final Target Link_Kitchen = new Target("Link_Kitchen","//button[@id= 'main-nav__button--kitchen']",Target.XPATH);	
	public static final Target Link_kitchenMainMenu = new Target("Link_kitchenMainMenu","//div[@id='main-nav']//following::div[@id='sub-nav-tab--kitchen']",Target.XPATH);	
	public static final Target Link_KitchenPage = new Target("Link_KitchenPage","//*[@id='sub-nav-tab--kitchen']//following::a[text()='Kitchen ']",Target.XPATH);	
	public static final Target Link_kitchenPlanner = new Target("Link_kitchenPlanner","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[3]/ul/li[5]/a",Target.XPATH);
	public static final Target Link_IdeaskitchenPlanner = new Target("Link_IdeaskitchenPlanner","//*[@id='sub-nav-tab--ideas']/div/div/div[3]/ul/li[5]/a",Target.XPATH);
	public static final Target Link_PressRoom = new Target("Link_PressRoom","//*[@id='footer__tab--1']/a[4]",Target.XPATH);
	public static final Target Link_FindaStore = new Target("Link_FindaStore","//*[@id='store-locatores']",Target.XPATH);
	public static final Target textbox_FindaStore = new Target("textbox_FindaStore","//*[@id='store-locator__search-field']",Target.XPATH);
	public static final Target button_FindaStore = new Target("button_FindaStore","//*[@id='store-locator__search-button-container']/div",Target.XPATH);
	public static final Target text_NoStoreLocator = new Target("text_NoStoreLocator","//*[@id='store-locator__num-results']/span[1]",Target.XPATH);
	public static final Target text_StoreLocator_num = new Target("text_StoreLocator_num","//*[@id='store-locator__num-results']",Target.XPATH);
	public static final Target text_StoreLocator_result = new Target("text_StoreLocator_result","//*[@id='store-locator__refine-search-tray']/div[2]/span[2]",Target.XPATH);
	public static final Target text_StoreLocator_location = new Target("text_StoreLocator_location","//*[@id='store-locator__location']",Target.XPATH);
	public static final Target count_StoreLocator_results = new Target("count_StoreLocator_results","//*[@id='store-locator__results']/ul/li",Target.XPATH);
	public static final Target Button_PrintCancelclick = new Target("Button_PrintCancelclick","//*[@id='print-header']/div/button[2]",Target.XPATH);
	public static final Target text_pressRoom = new Target("text_pressRoom","//*[@class='container']//following::div[@class='press-room-section press-room-intro']/h2",Target.XPATH);
	public static final Target Link_Ideas = new Target("Link_Ideas","main-nav__button--ideas ",Target.ID);	
	public static final Target Link_ideasMainMenu = new Target("Link_ideasMainMenu","//div[@id='main-nav']//following::div[@id='sub-nav-tab--ideas']",Target.XPATH);	
	public static final Target Link_IdeasPage = new Target("Link_IdeasPage","//*[@id='sub-nav-tab--ideas']//following::a[text()='Inspiration']",Target.XPATH);	
	
	public static final Target Link_HelpUsToImproveMore = new Target("link_HelpUsToImpMore","kampylink",Target.ID);	
	public static final Target text_commenttextbox = new Target("text_commenttextbox","//textarea[@id='textarea']",Target.XPATH);	
	public static final Target btn_sendmyComments = new Target("btn_comments","//button[@class='submitButton']",Target.XPATH);	
	public static final Target btn_suggestion = new Target("btn_Sugg","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[1]/label/span/span",Target.XPATH);	
	public static final Target btn_dislike = new Target("btn_dlike","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[2]/label/span/span",Target.XPATH);	
	public static final Target btn_praise = new Target("btn_praise","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[3]/label/span/span",Target.XPATH);	
	public static final Target btn_Rating = new Target("btn_rating","//*[@id= 'ItemId-35770-2']",Target.XPATH);	
	public static final Target btn_closeTheWind = new Target("btn_closeTheWind","//*[@id= 'Finish']",Target.XPATH);	
	
	public static final Target link_NewsLetterSignUp = new Target("link_NewsLetterSignUp","//*[@id='footer__tab--1']/a[5]",Target.XPATH);	
	public static final Target text_Email = new Target("text_Email","//*[@id='field0']",Target.XPATH);	
	public static final Target text_FirstName = new Target("text_FirstName","//*[@id='field1']",Target.XPATH);	
	public static final Target text_LastName = new Target("btn_closeTheWind","//*[@id='field2']",Target.XPATH);	
	public static final Target text_PostalCode = new Target("btn_closeTheWind","//*[@id='field7']",Target.XPATH);	
	public static final Target select_Country = new Target("select_Country","//*[@id='field8']",Target.XPATH);	
	public static final Target select_occupation = new Target("select_occupation","//*[@id='field9']",Target.XPATH);	
	public static final Target button_interiorProducts = new Target("button_KitchenBath","//*[@id='heading2']//following::a",Target.XPATH);	
	public static final Target Div_ExpandedFields = new Target("Div_ExpandedFields","//*[@id='collapse2']",Target.XPATH);	
	public static final Target checkbox_interiorProducts = new Target("checkbox_interiorProducts","//*[@id='formElement22']//following::div[@class='check-field']/label",Target.XPATH);	
	public static final Target button_Submit = new Target("button_Submit","//*[@id='form42']//following::div[@class='pull-right']/button",Target.XPATH);	
	public static final Target text_thankyou = new Target("text_thankyou","/html/body/center/p",Target.XPATH);	
	
	public static final Target Link_Parts = new Target("Link_Parts","//button[@id='main-nav__button--parts']",Target.XPATH);	
	public static final Target Link_PartsMainMenu = new Target("Link_PartsMainMenu","//div[@id='main-nav']//following::div[@id='sub-nav-tab--parts']",Target.XPATH);	
	public static final Target Link_PartsPage = new Target("Link_PartsPage","//*[@class='sub-nav-tab__title display-none-sm-only']",Target.XPATH);	
	public static final Target Link_PartsWizard = new Target("Link_PartsWizard","//*[@class='btn btn-hero hidden-xs']",Target.XPATH);	
	public static final Target button_ViewResults = new Target("button_ViewResults","//*[@id='question-features']//following::a",Target.XPATH);	
	public static final Target button_ViewResultsCount = new Target("button_ViewResultsCount","//*[@id='question-features']//following::a/span",Target.XPATH);	
	public static final Target Count_Results = new Target("Count_Results","//*[@id='finder-results']//following::span[1]",Target.XPATH);	
	
	public static final Target link_FindAPro = new Target("link_FindAPro","//*[@id='pro-locatores']",Target.XPATH);	
	public static final Target Ele_NeedHelpWith = new Target("Ele_NeedHelpWith","//*[@id='pro-locatores']",Target.XPATH);
	               
	public static final Target Ele_CustomerSupport = new Target("Ele_CustomerSupport","//*[@id='pro-locatores']",Target.XPATH);	
	public static final Target EleValue_NeedHelpWith = new Target("EleValue_NeedHelpWith","//*[@id='pro-finder']//following::div[@class='col-6-lg col-12-md col-block col-block-accordion']/h3",Target.XPATH);	
	public static final Target button_CustSupport = new Target("button_CustSupport","//*[@id='pro-finder']//following::div[@class='col-6-lg col-12-md col-block marg-t-25-sm marg-t-0-lg marg-b-50-sm']/button",Target.XPATH);	
	public static final Target button_NeedHelpWithSubmit = new Target("button_NeedHelpWithSubmit","//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']",Target.XPATH);	
	public static final Target button_getDesignHelp = new Target("button_getDesignHelp","//*[@id='BDH']/div/div/button",Target.XPATH);	
	public static final Target button_KohlerProductInst = new Target("button_KohlerProductInst","//div[@class='accordion marg-t-30-sm row']//following::div[@class='accordion-item col-12-lg']/button/header/h3",Target.XPATH);	
	public static final Target text_FindAProErrorMsg = new Target("text_FindAProErrorMsg","//div[@class='container no-results-find-a-pro']//following::div[@class='p10 form-help-text marg-b-40-sm']/p",Target.XPATH);	
	public static final Target Link_GoBack = new Target("Link_GoBack","//a[@class='back-results-button']",Target.XPATH);	
	public static final Target Logo=new Target("Logo","/html/body/header/div/div[1]/div[1]/a",Target.XPATH);
	public static final Target Header=new Target("Header","//*[@id=\"pro-finder\"]/div/h1",Target.XPATH);
	
	public static final Target text_FAPPageHeader = new Target("text_FAPPageHeader","//div[@class='contact-forms-basic marg-t-50-sm']//following::div[@class='main-header']/h1",Target.XPATH);	
	public static final Target button_GetAnEstm = new Target("button_GetAnEstm","//div[@class='col-4-md why-certified-block marg-t-45-md marg-t-0-sm']//following::button",Target.XPATH);	
	public static final Target result_Address = new Target("result_Address","//div[@class='row pad-l-45-md pad-r-45-md pad-l-0-lg pad-r-0-lg']/div/h3",Target.XPATH);	
	public static final Target text_getAnEstmHeader = new Target("text_getAnEstmHeader","//div[@class='contact-forms-basic marg-t-50-sm']//following::h1",Target.XPATH);	
	
	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
	JavascriptExecutor js = (JavascriptExecutor)getCommand().driver;

	String browserName=caps.getBrowserName();

	public Kohler_GeneralNavigation(SiteRepository repository)
	{
		super(repository);
	}

	public Kohler_GeneralNavigation _GoToGeneralNavigation()
	{
		log("navigate to general navigation page",LogType.STEP);
		//getCommand().captureScreenshot("C:\\Users\\Arvind01\\Desktop\\Add To Cart\\HomePage.png");
		return this;
		
	}
	
	
	// verify Bathroom expansion and bathroom links
		public Kohler_GeneralNavigation VerifyBathroomMainMenu()
		{
			
			try {			    
					getCommand().waitForTargetPresent(Link_bathroom);
					getCommand().click(Link_bathroom);
					
				
					if(getCommand().isTargetVisible(Link_bathroomMainMenu))
						Assert.assertTrue(true, "Bathroom main menu is expanded successfully");
					
					getCommand().isTargetVisible(Link_bathroomPage);
					
					int Win_size = getCommand().driver.getWindowHandles().size();
					log("Open Windows Size is :" + Win_size ,LogType.STEP);
					
					log(getCommand().getText(Link_bathroomPage),LogType.STEP);
					
					if(browserName.equals("safari"))
					{
						String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN); 				
						getCommand().sendKeys(Link_bathroomPage, selectLinkOpeninNewTab);	

					}
					else
					{
						String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 				
						getCommand().sendKeys(Link_bathroomPage, selectLinkOpeninNewTab);	

					}

				
					WebDriverWait wait = new WebDriverWait(getCommand().driver,25);
					wait.until(ExpectedConditions.numberOfwindowsToBe(2));
					ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
					
					String pageURl = getCommand().driver.getCurrentUrl(); 
					
					if(browserName.equals("safari"))
					{

						getCommand().driver.switchTo().window(tabs2.get(0));
					}
	    		 else
	    		 {
						getCommand().driver.switchTo().window(tabs2.get(1));

	    		 }
				    wait = new WebDriverWait(getCommand().driver,25);
					wait.until(ExpectedConditions.titleContains("Bathroom | KOHLER"));
					
					String pageTitle = getCommand().driver.getTitle();
				    log("Page title is: " + pageTitle,LogType.STEP);
				    getCommand().waitFor(3);
				    Assert.assertEquals(pageTitle, "Bathroom | KOHLER", "Page is not navigated to bathroom Page");
				    
				    List<WebElement> Bathroom_links = getCommand().driver.findElements(By.xpath("//*[@id='section']/div"));
				    int link_count = Bathroom_links.size();
				    
				    for (int i = 3; i <= link_count; i++)
				    {
					
				    	WebElement link = getCommand().driver.findElement(By.xpath("//*[@id='section']/div["+i+"]/div/div[1]/a"));
				    	log("Link text is :" + link.getText(),LogType.STEP);
				    	
						log("Click on link in Bathroom homepage",LogType.STEP);
						
						if(browserName.equals("safari"))
						{
							link.click();
							
							
							String BathroompageURl = getCommand().driver.getCurrentUrl(); 
							 String CurrentpageURl = getCommand().driver.getCurrentUrl();
							    log("verify the Page title"+ CurrentpageURl,LogType.STEP);
							    
							    if(!CurrentpageURl.isEmpty() && !CurrentpageURl.equals(pageURl) && !CurrentpageURl.equals(BathroompageURl) )
								    Assert.assertTrue(true, "Navigated to respective Page");
							    

							getCommand().goBack();
							getCommand().waitFor(2);

						}
						else
						{
							String LinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 				
							link.sendKeys(LinkOpeninNewTab);		

						
						
						wait = new WebDriverWait(getCommand().driver,25);
						wait.until(ExpectedConditions.numberOfwindowsToBe(3));
						ArrayList<String> tabs3 = new ArrayList<String> (getCommand().driver.getWindowHandles());
						
						String BathroompageURl = getCommand().driver.getCurrentUrl(); 
						
								getCommand().driver.switchTo().window(tabs3.get(2));

			    		
						getCommand().waitFor(3);
					    Win_size = getCommand().driver.getWindowHandles().size();
					    Assert.assertEquals(3, Win_size, "On clicking on link, the page is not opned in new window");
					    
					    String CurrentpageURl = getCommand().driver.getCurrentUrl();
					    log("verify the Page title"+ CurrentpageURl,LogType.STEP);
					    							
					    if(!CurrentpageURl.isEmpty() && !CurrentpageURl.equals(pageURl) && !CurrentpageURl.equals(BathroompageURl) )
					    Assert.assertTrue(true, "Navigated to respective Page");

					    getCommand().driver.close();
					    
					   
		    		 
					    	getCommand().driver.switchTo().window(tabs3.get(1));

		    		 }
					    
					    
					    
				    	
					}

				       	getCommand().driver.close();
				       	
				        if(browserName.equals("safari"))
						{

					    	getCommand().driver.switchTo().window(tabs2.get(1));
						}
		    		 else
		    		 {
					    	getCommand().driver.switchTo().window(tabs2.get(0));

		    		 }
					    
				
				
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			
			
			return this;
		}
	// verify Bathroom menu links and link navigation
	public Kohler_GeneralNavigation verifyBathroomSubMenuLinks()
	{
		try{

			getCommand().waitForTargetPresent(Link_bathroom);
			getCommand().click(Link_bathroom);
					
			if(getCommand().isTargetVisible(Link_bathroomMainMenu))
				Assert.assertTrue(true, "Bathroom main menu is expanded successfully");
			
			List<WebElement> Bathroom_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--bathroom']/div/div/div"));
		    int link_count = Bathroom_SubMenuLinks.size();
		    
		    for (int i = 1; i <= link_count; i++)
		    {
		    	String title =getCommand().driver.findElement(By.xpath("//*[@id='sub-nav-tab--bathroom']/div/div/div["+i+"]/div[1]")).getText();
		    	if(title.contains("Products"))
		    	{
		    		List<WebElement> Bathroom_ProductsLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--bathroom']/div/div/div["+i+"]/div[2]/div"));
			    	int Products_count = Bathroom_ProductsLinks.size();
			    	for (int j = 1; j <= Products_count; j++) 
			    	{
					
			    		List<WebElement> Bathroom_Pro = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--bathroom']/div/div/div["+i+"]/div[2]/div["+j+"]/ul/li/a"));
				    	int Pro_count = Bathroom_Pro.size();
				    	for (int k = 0; k < Pro_count; k++)
				    	{
							
				    		log("verify the link text"+  Bathroom_Pro.get(k).getText(),LogType.STEP);
							
							log("click on each link and verify in new tab",LogType.STEP);
							
							if(browserName.equals("safari"))
							{
								String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN); 
								Bathroom_Pro.get(k).sendKeys(selectLinkOpeninNewTab);

							}
							else
							{
								String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
								Bathroom_Pro.get(k).sendKeys(selectLinkOpeninNewTab);

							}
							
						
							WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
							wait.until(ExpectedConditions.numberOfwindowsToBe(2));
							ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
						
							String pageURl = getCommand().driver.getCurrentUrl(); 
							
							
							 if(browserName.equals("safari"))
								{

									getCommand().driver.switchTo().window(tabs2.get(0));
								}
				                else
				                {
									getCommand().driver.switchTo().window(tabs2.get(1));

				                }

							
							getCommand().waitFor(3);									
						    int Win_size = getCommand().driver.getWindowHandles().size();
						    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opened in new window");
						    
							String CurrentpageURl = getCommand().driver.getCurrentUrl();
							log("verify the Page title"+ CurrentpageURl,LogType.STEP);
							
							if(!CurrentpageURl.isEmpty() && !pageURl.equals(CurrentpageURl))
								Assert.assertTrue(true, "Navigated to respective Page");
							
							getCommand().driver.close();
							log("Close the tab and switch back to parent window",LogType.STEP);
							
							 if(browserName.equals("safari"))
								{

									getCommand().driver.switchTo().window(tabs2.get(1));
								}
				                else
				                {
									getCommand().driver.switchTo().window(tabs2.get(0));

				                }
							
						
				    		
						}
					}
		    	}
		    	else
		    	{
		    		List<WebElement> Bathroom_FeatureLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--bathroom']/div/div/div["+i+"]/ul/li"));
		    		int FeaLinks_count = Bathroom_FeatureLinks.size();
		    		for (int m = 1; m <= FeaLinks_count; m++)
		    		{
		    			List<WebElement> Bathroom_InnerLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--bathroom']/div/div/div["+i+"]/ul/li["+m+"]/a"));
			    		int FeaInnerLinks_count = Bathroom_InnerLinks.size();
			    		for (int n = 0; n < FeaInnerLinks_count; n++) 
			    		{
							
							log("verify the link text"+  Bathroom_InnerLinks.get(n).getText(),LogType.STEP);
							
							log("click on each link and verify in new tab",LogType.STEP);
							
							if(browserName.equals("safari"))
							{
								String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN); 
								Bathroom_InnerLinks.get(n).sendKeys(selectLinkOpeninNewTab);

							}
							else
							{
								String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
								Bathroom_InnerLinks.get(n).sendKeys(selectLinkOpeninNewTab);
							}
							
						
							
							
							
							getCommand().waitFor(5);
							ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
							
							
							
							 if(browserName.equals("safari"))
								{

									getCommand().driver.switchTo().window(tabs2.get(0));
								}
				    		 else
				    		 {
									getCommand().driver.switchTo().window(tabs2.get(1));

				    		 }
							
							
							
						    int Win_size = getCommand().driver.getWindowHandles().size();
						    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
						    
							String pageTitle = getCommand().driver.getTitle();
							log("verify the Page title"+ pageTitle,LogType.STEP);
							
							getCommand().driver.close();
							log("Close the tab and switch back to parent window",LogType.STEP);
							
							 if(browserName.equals("safari"))
								{

									getCommand().driver.switchTo().window(tabs2.get(1));
								}
				    		 else
				    		 {
									getCommand().driver.switchTo().window(tabs2.get(0));

				    		 }

							
							
						}
					}
		    	}
		    	
			}
			
			
			
			}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// Verify choreograph Shower Planner link functionality+ new Window
	public Kohler_GeneralNavigation verifychoreographShowerPlanner()
	{
		try
		{
			log("Verify choreograph Shower Planner link" ,LogType.STEP);
			getCommand().waitForTargetPresent(Link_bathroom);
			getCommand().click(Link_bathroom);
		
			if(getCommand().isTargetVisible(Link_bathroomMainMenu))
				Assert.assertTrue(true, "Bathroom main menu is expanded successfully");
			
			int Win_size = getCommand().driver.getWindowHandles().size();
			log("Open Windows Size is :" + Win_size ,LogType.STEP);
			
			getCommand().waitForTargetPresent(Link_ChoregraphShowerPlanner);
			getCommand().click(Link_ChoregraphShowerPlanner);
			WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
			wait.until(ExpectedConditions.numberOfwindowsToBe(2));
			ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			
			 if(browserName.equals("safari"))
				{

					getCommand().driver.switchTo().window(tabs.get(0));
				}
 		 else
 		 {
 			getCommand().driver.switchTo().window(tabs.get(1));

 		 }

		
//			wait = new WebDriverWait(getCommand().driver,100);
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='kohler_link']")));
			getCommand().waitFor(3);
			log("Page URL is: " + getCommand().getPageUrl(),LogType.STEP);
		    Win_size = getCommand().driver.getWindowHandles().size();
		    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
		    
		    log("Close the Child Window: "+getCommand().driver.getTitle(),LogType.STEP);
		    getCommand().driver.close();
		    
		    if(browserName.equals("safari"))
			{

				getCommand().driver.switchTo().window(tabs.get(1));
			}
		 else
		 {
		    	getCommand().driver.switchTo().window(tabs.get(0));

		 }
		    
			   	
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	
		
	}

	// verify bathroom Product Buying guide and Article page
	public Kohler_GeneralNavigation VerifyBathroomProductBuyingGuide()
	{
		String browserName=caps.getBrowserName();

		try 
		{
			List<String> ShareLinkTextItems = new ArrayList<String>();
			String Share_linkText=null;
			log("Verify Product Buying Guide link" ,LogType.STEP);
			getCommand().waitForTargetPresent(Link_bathroom);
			getCommand().click(Link_bathroom);
		
			if(getCommand().isTargetVisible(Link_bathroomMainMenu))
				Assert.assertTrue(true, "Bathroom main menu is expanded successfully");
			
			log("Verify Article page on clicking on PBG link" ,LogType.STEP);
			getCommand().waitForTargetPresent(Link_BathroomProductBuyingGuide);
			getCommand().click(Link_BathroomProductBuyingGuide);
			
			getCommand().waitFor(5);
			
			boolean condtion= true;
			log("Verify the Article Page URL",LogType.STEP);
			String URL = getCommand().driver.getCurrentUrl();
			if(URL.contains("article"))
			Assert.assertTrue(condtion);
			
			List<WebElement> buttons = getCommand().driver.findElements(By.xpath("//*[@id='article-page']//following::div[@class='article-page__icon-container']/button/i"));
			for(WebElement button: buttons)
			{
				String text_button = button.getAttribute("class");
				String text = text_button.replaceFirst("icon--", "");
				
				switch (text)
				{
				case "bookmark":
					
					button.click();
					getCommand().isTargetVisible(Header_SaveToFolder);
					String Header_text = getCommand().getText(Header_SaveToFolder);
				
					if(browserName.equals("MicrosoftEdge") || browserName.equals("safari"))
					{
						Assert.assertEquals(Header_text,"Save To MY FOLDERS", "Application is not navigated to Save to folder window");
						
					}
					else{
						Assert.assertEquals(Header_text,"SAVE TO MY FOLDERS", "Application is not navigated to Save to folder window");
					}
										
					getCommand().isTargetVisible(textBox_SaveToFolder);
					getCommand().sendKeys(textBox_SaveToFolder, "Test");
					
					getCommand().isTargetVisible(button_SaveToFolderSave);
					getCommand().click(button_SaveToFolderSave);
					getCommand().waitFor(2);
					
					if(getCommand().isTargetVisible(Win_ShowThankYou))
						Assert.assertTrue(true, "Thank you page is expanded successfully");
					
					getCommand().isTargetVisible(title_ItemAddedToFold1);
					String title1= getCommand().getText(title_ItemAddedToFold1);
					
					getCommand().isTargetVisible(title_ItemAddedToFold2);
					String title2= getCommand().getText(title_ItemAddedToFold2);
					
					String title = title1+" "+title2;
					Assert.assertEquals(title,"Your item was successfully added to My Kohler Folder", "Application is not navigated to My Kohler folder");
					
					getCommand().isTargetVisible(button_ItemAddedToFoldClose);
					getCommand().click(button_ItemAddedToFoldClose);
					
					break;
					
				case "share":
					button.click();
					getCommand().isTargetVisible(Items_shareItems);
					List<WebElement> shareLinks = getCommand().driver.findElements(By.xpath("//*[@class='share-tip organic-namespace olbox-container']/div/ul/li/a"));
					for(WebElement linkText: shareLinks)
					{
						 Share_linkText = linkText.getText();
						 log("Share options are : " + Share_linkText,LogType.STEP);
					}
					ShareLinkTextItems.add(Share_linkText);
					
					getCommand().isTargetVisible(Items_shareItemFaceebook);
					getCommand().click(Items_shareItemFaceebook);
					getCommand().waitFor(5);
					
					ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
					
					 if(browserName.equals("safari"))
						{

							getCommand().driver.switchTo().window(tabs.get(0));
						}
		    		 else
		    		 {
							getCommand().driver.switchTo().window(tabs.get(1));

		    		 }
					
					
				    log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
				    int Win_size = getCommand().driver.getWindowHandles().size();
				    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
				    
				    log("Close the Child Window: "+getCommand().driver.getTitle(),LogType.STEP);
				    getCommand().driver.close();
				    getCommand().waitFor(3);
				    if(browserName.equals("safari"))
					{

						getCommand().driver.switchTo().window(tabs.get(1));
					}
	    		 else
	    		 {
				    	getCommand().driver.switchTo().window(tabs.get(0));

	    		 }
				    
					
			    	break;
				case "print":
					
					break;

				default:
					throw new Exception("The type of functionality is neither of the above.");
					
				}
				
				
				}
	
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// verify Kitchen expansion and bathroom links
	public Kohler_GeneralNavigation VerifyKitchenMainMenu()
	{
			
		try {		
					log("verify Kitchen Main menu",LogType.STEP);
					getCommand().waitForTargetPresent(Link_Kitchen);
					getCommand().click(Link_Kitchen);
									
					if(getCommand().isTargetVisible(Link_kitchenMainMenu))
						Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
					
					getCommand().isTargetVisible(Link_KitchenPage);
					
					int Win_size = getCommand().driver.getWindowHandles().size();
					log("Open Windows Size is :" + Win_size ,LogType.STEP);
					
					log(getCommand().getText(Link_KitchenPage),LogType.STEP);
					
					if(browserName.equals("safari"))
					{
					String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN); 				
					getCommand().sendKeys(Link_KitchenPage, selectLinkOpeninNewTab);	

					}
					else
					{
						String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 				
						getCommand().sendKeys(Link_KitchenPage, selectLinkOpeninNewTab);	

					}
					
					
					WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
					wait.until(ExpectedConditions.numberOfwindowsToBe(2));
					ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
					
					String pageURl = getCommand().driver.getCurrentUrl(); 
					
					 if(browserName.equals("safari"))
						{

							getCommand().driver.switchTo().window(tabs2.get(0));
						}
		    		 else
		    		 {
							getCommand().driver.switchTo().window(tabs2.get(1));

		    		 }

					
					
					getCommand().waitFor(10);
					
				    String pageTitle = getCommand().driver.getTitle();
				    wait = new WebDriverWait(getCommand().driver,10);
					wait.until(ExpectedConditions.titleContains("Kitchen | KOHLER"));
				    log("Page title is:" + pageTitle,LogType.STEP);
				    
				    Assert.assertEquals(pageTitle, "Kitchen | KOHLER", "Page is not navigated to Kitchen Page");
				    
				    List<WebElement> Kitchen_links = getCommand().driver.findElements(By.xpath("//*[@id='section']/div"));
				    int link_count = Kitchen_links.size();
				    
				    for (int i = 3; i <= link_count; i++)
				    {
					
				    	WebElement link = getCommand().driver.findElement(By.xpath("//*[@id='section']/div["+i+"]/div/div[1]/a"));
				    	log("Link text is :" + link.getText(),LogType.STEP);
				    	
						log("Click on link in Kitchen homepage",LogType.STEP);

						
						
						if(browserName.equals("safari"))
						{
							link.click();
							String KitchenpageURl = getCommand().driver.getCurrentUrl(); 
							 String CurrentpageURl = getCommand().driver.getCurrentUrl();
							    log("verify the Page title"+ CurrentpageURl,LogType.STEP);
							    
							    if(!CurrentpageURl.isEmpty() && !CurrentpageURl.equals(pageURl) && !CurrentpageURl.equals(KitchenpageURl) )
								    Assert.assertTrue(true, "Navigated to respective Page");
							    

							getCommand().goBack();
							getCommand().waitFor(2);

						}
					else
						{
							String LinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 				
							link.sendKeys(LinkOpeninNewTab);	

						
						
						
						
						wait = new WebDriverWait(getCommand().driver,10);
						wait.until(ExpectedConditions.numberOfwindowsToBe(3));
						ArrayList<String> tabs3 = new ArrayList<String> (getCommand().driver.getWindowHandles());
						
						String KitchenpageURl = getCommand().driver.getCurrentUrl(); 
						
						
			    		 
								getCommand().driver.switchTo().window(tabs3.get(2));

			    		
						
						getCommand().waitFor(3);
					    Win_size = getCommand().driver.getWindowHandles().size();
					    Assert.assertEquals(3, Win_size, "On clicking on link, the page is not opned in new window");
					    
					    String CurrentpageURl = getCommand().driver.getCurrentUrl();
					    log("verify the Page title"+ CurrentpageURl,LogType.STEP);
					    
					    if(!CurrentpageURl.isEmpty() && !CurrentpageURl.equals(pageURl) && !CurrentpageURl.equals(KitchenpageURl))
						    Assert.assertTrue(true, "Navigated to respective Page");
					    if(browserName.equals("chrome") || browserName.equals("firefox") || browserName.equals("edge"))
					    {
				    
					    
					    getCommand().driver.close();
					    
					    

					    	getCommand().driver.switchTo().window(tabs3.get(1));

		    	

					    }
				
					}
				    
				    }
				       	getCommand().driver.close();
				       	
				        if(browserName.equals("safari"))
						{

					    	getCommand().driver.switchTo().window(tabs2.get(1));
						}
		    		 else
		    		 {
					    	getCommand().driver.switchTo().window(tabs2.get(0));

		    		 }
	
	
				
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			
			
			return this;
		}
		
	// verify Kitchen menu links and link navigation
	public Kohler_GeneralNavigation verifyKitchenSubMenuLinks()
	{
		try{
			
			getCommand().waitForTargetPresent(Link_Kitchen);
			getCommand().click(Link_Kitchen);
		
			if(getCommand().isTargetVisible(Link_kitchenMainMenu))
				Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
			
			List<WebElement> Kitchen_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div"));
		    int link_count = Kitchen_SubMenuLinks.size();
		    
		    for (int i = 1; i <= link_count; i++)
		    {
		    		List<WebElement> Kitchen_Links = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div["+i+"]/ul/li"));
		    		int Links_count = Kitchen_Links.size();
		    		for (int m = 1; m <= Links_count; m++)
		    		{
		    			List<WebElement> Kitchen_InnerLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div["+i+"]/ul/li["+m+"]/a"));
			    		int InnerLinks_count = Kitchen_InnerLinks.size();
			    		for (int n = 0; n < InnerLinks_count; n++) 
			    		{
							log("verify the link text"+  Kitchen_InnerLinks.get(n).getText(),LogType.STEP);
							
							log("click on each link and verify in new tab",LogType.STEP);
							
							if(browserName.equals("safari"))
							{
								String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN); 
								Kitchen_InnerLinks.get(n).sendKeys(selectLinkOpeninNewTab);

							}
							else
							{
								String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
								Kitchen_InnerLinks.get(n).sendKeys(selectLinkOpeninNewTab);

							}
							
						
							
							WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
							wait.until(ExpectedConditions.numberOfwindowsToBe(2));
							ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
							
							String pageURl = getCommand().driver.getCurrentUrl(); 
							

				    		 if(browserName.equals("safari"))
								{

									getCommand().driver.switchTo().window(tabs2.get(0));
								}
				    		 else
				    		 {
									getCommand().driver.switchTo().window(tabs2.get(1));

				    		 }

						
							
							getCommand().waitFor(3);
																							
							int Win_size = getCommand().driver.getWindowHandles().size();
							Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
						    
							String CurrentpageURl = getCommand().driver.getCurrentUrl();
							log("verify the Page title: "+ CurrentpageURl,LogType.STEP);
														
							if(!CurrentpageURl.isEmpty() && !pageURl.equals(CurrentpageURl))
							Assert.assertTrue(true, "Navigated to respective Page");
							
							getCommand().driver.close();
							log("Close the tab and switch back to parent window",LogType.STEP);
							 if(browserName.equals("safari"))
								{

									getCommand().driver.switchTo().window(tabs2.get(1));
								}
				    		 else
				    		 {
									getCommand().driver.switchTo().window(tabs2.get(0));

				    		 }
						}
					}
		    	
		    }
			
			
			
			}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// Verify kitchen Planner link functionality+ new Window
	public Kohler_GeneralNavigation verifyKitchenPlanner()
	{
		try
		{
			log("Verify Kitchen Planner link" ,LogType.STEP);
			getCommand().waitForTargetPresent(Link_Kitchen);
			getCommand().click(Link_Kitchen);
		
			if(getCommand().isTargetVisible(Link_kitchenMainMenu))
				Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
			
			int Win_size = getCommand().driver.getWindowHandles().size();
			log("Open Windows Size is :" + Win_size ,LogType.STEP);
			
			getCommand().waitForTargetPresent(Link_kitchenPlanner);
			getCommand().click(Link_kitchenPlanner);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver,25);
			wait.until(ExpectedConditions.numberOfwindowsToBe(2));
			ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			
   		 if(browserName.equals("safari"))
				{

 			getCommand().driver.switchTo().window(tabs.get(0));
				}
   		 else
   		 {
 			getCommand().driver.switchTo().window(tabs.get(1));

   		 }

//			wait = new WebDriverWait(getCommand().driver,100);
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("footer-title")));
			getCommand().waitFor(3);
			log("Page URL is: " + getCommand().getPageUrl(),LogType.STEP);
		    Win_size = getCommand().driver.getWindowHandles().size();
		    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
		    
		    log("Close the Child Window: "+getCommand().getPageUrl(),LogType.STEP);
		    getCommand().driver.close();
		    

	   		 if(browserName.equals("safari"))
					{

	 	    	getCommand().driver.switchTo().window(tabs.get(1));
					}
	   		 else
	   		 {
	 	    	getCommand().driver.switchTo().window(tabs.get(0));

	   		 }
		
			   	
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	
		
	}

	// verify Press room link/ functionality
	public Kohler_GeneralNavigation VerifyPressRoomLink()
	{
		try {
			
			log("Verify PressRoom link" ,LogType.STEP);
			getCommand().isTargetVisible(Link_PressRoom);
			getCommand().click(Link_PressRoom);
			
			log("Verify the Press Room Page Title",LogType.STEP);
			getCommand().waitFor(2);
			Assert.assertEquals(getCommand().driver.getTitle() ,"Press Room | KOHLER","Home Page Title mismatch");
			
			getCommand().isTargetPresent(text_pressRoom);
			log("Verify the Press Room Page Header:"+getCommand().getText(text_pressRoom) ,LogType.STEP);
			Assert.assertEquals(getCommand().getText(text_pressRoom),"Press Room" ,"Page header mis matches");
			
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	// verify Find a store functionality
	public Kohler_GeneralNavigation VerifyFindAStore()
	{
		try
		{
			List<String> StoreLocationResults = new ArrayList<String>();
			String resultName=null;
			log("Verify Find a Store link" ,LogType.STEP);
			getCommand().isTargetVisible(Link_FindaStore);
			getCommand().click(Link_FindaStore);
			
			log("Verify the Find a Store Page Title",LogType.STEP);
			getCommand().waitFor(3);
			Assert.assertEquals(getCommand().driver.getTitle() ,"Results | Find a Store | KOHLER","Home Page Title mismatch");
			
			log("Verify store locator text" ,LogType.STEP);
			getCommand().isTargetVisible(text_NoStoreLocator);
			String storeLocatorText = getCommand().getText(text_NoStoreLocator);
			String expectedStoreLocText= "We are sorry, but we have no stores within 250 miles of your location. Please try another search.";
			
			if(!storeLocatorText.equalsIgnoreCase(expectedStoreLocText))
			{
				log("Store is within the radius of your location",LogType.STEP);
				String StoreLocator_text = getCommand().getText(text_StoreLocator_num) + getCommand().getText(text_StoreLocator_result) + " for " + getCommand().getText(text_StoreLocator_location);
				log("Store is within the radius and results are: "+ StoreLocator_text ,LogType.STEP);
				getCommand().isTargetVisible(count_StoreLocator_results);
				Assert.assertEquals(getCommand().getText(text_StoreLocator_num).toString(), getCommand().getTargetCount(count_StoreLocator_results), "Store locator count is not matching with store locator results");
				
				List<WebElement> storeLocator_results = getCommand().driver.findElements(By.xpath("//*[@id='store-locator__results']/ul/li"));
				int count_results = storeLocator_results.size();
				
				for (int i = 0; i < count_results; i++)
				{
					
					resultName= getCommand().driver.findElement(By.xpath("//*[@id='store-locator__results']/ul/li["+i+"]/a")).getText();
					log("Store results name is: "+ resultName  ,LogType.STEP);
					StoreLocationResults.add(resultName);	
					
				}
						
			
			}
			else
			{
				log("Store is not within the radius of your location",LogType.STEP);
			}
			
			log("Input a value in search box to find a store location" ,LogType.STEP);
			getCommand().isTargetVisible(textbox_FindaStore);
			getCommand().clear(textbox_FindaStore);
			getCommand().sendKeys(textbox_FindaStore , "ohio");
			getCommand().isTargetVisible(button_FindaStore);
			getCommand().click(button_FindaStore);
			getCommand().waitFor(5);
			
			log("Verify store locator text" ,LogType.STEP);
			getCommand().isTargetVisibleAfterWait(text_StoreLocator_num);
			storeLocatorText = getCommand().getText(text_StoreLocator_num);
			
			
			
			if(!storeLocatorText.equalsIgnoreCase(expectedStoreLocText))
			{
				log("Store is within the radius of your location",LogType.STEP);
				String StoreLocator_text = storeLocatorText +" "+ getCommand().getText(text_StoreLocator_result) + " for " + getCommand().getText(text_StoreLocator_location);
				log("Store is within the radius and results are: "+ StoreLocator_text ,LogType.STEP);

				getCommand().isTargetVisible(count_StoreLocator_results);
				int count =getCommand().getTargetCount(count_StoreLocator_results);
				String countIs = String.valueOf(count);
				Assert.assertEquals(storeLocatorText.toString(),countIs , "Store locator count is not matching with store locator results");
				
				List<WebElement> storeLocator_results = getCommand().driver.findElements(By.xpath("//*[@id='store-locator__results']/ul/li"));
				int count_results = storeLocator_results.size();
				
				for (int i = 1; i <= count_results; i++)
				{
					
					resultName= getCommand().driver.findElement(By.xpath("//*[@id='store-locator__results']/ul/li["+i+"]/a")).getText();
					log("Store results name is: "+ resultName  ,LogType.STEP);
					StoreLocationResults.add(resultName);
					
				}
							
			
			}
			else
			{
				log("Store is not within the radius of your location",LogType.STEP);
				Assert.fail("Unable to find a store within the radius");
			}
			
				
		}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			return this;
	}

	// verify bathroom Product Buying guide and Article page
	public Kohler_GeneralNavigation VerifyKitchenProductBuyingGuide()
	{
		String browserName = caps.getBrowserName();

		try 
		{
			List<String> ShareLinkTextItems = new ArrayList<String>();
			String Share_linkText=null;
			log("Verify Product Buying Guide link" ,LogType.STEP);
			getCommand().waitForTargetPresent(Link_Kitchen);
			getCommand().click(Link_Kitchen);
		
			if(getCommand().isTargetVisible(Link_kitchenMainMenu))
				Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
			
			log("Verify Article page on clicking on PBG link" ,LogType.STEP);
			getCommand().waitForTargetPresent(Link_KitchenProductBuyingGuide);
			getCommand().click(Link_KitchenProductBuyingGuide);
			
			getCommand().waitFor(5);
			
			boolean condtion= true;
			log("Verify the Article Page URL",LogType.STEP);
			String URL = getCommand().driver.getCurrentUrl();
			if(URL.contains("article"))
			Assert.assertTrue(condtion);
			
			List<WebElement> buttons = getCommand().driver.findElements(By.xpath("//*[@id='article-page']//following::div[@class='article-page__icon-container']/button/i"));
			for(WebElement button: buttons)
			{
				String text_button = button.getAttribute("class");
				String text = text_button.replaceFirst("icon--", "");
				
				switch (text)
				{
				case "bookmark":
					
					button.click();
					getCommand().isTargetVisible(Header_SaveToFolder);
					String Header_text = getCommand().getText(Header_SaveToFolder);
					if(browserName.equals("MicrosoftEdge") || browserName.equals("safari"))
					{
						Assert.assertEquals(Header_text,"Save To MY FOLDERS", "Application is not navigated to Save to folder window");
						
					}
					else{
						Assert.assertEquals(Header_text,"SAVE TO MY FOLDERS", "Application is not navigated to Save to folder window");
					}
					
					getCommand().isTargetVisible(textBox_SaveToFolder);
					getCommand().sendKeys(textBox_SaveToFolder, "Test");
					
					getCommand().isTargetVisible(button_SaveToFolderSave);
					getCommand().click(button_SaveToFolderSave);
					getCommand().waitFor(2);
					
					if(getCommand().isTargetVisible(Win_ShowThankYou))
						Assert.assertTrue(true, "Thank you page is expanded successfully");
					
					getCommand().isTargetVisible(title_ItemAddedToFold1);
					String title1= getCommand().getText(title_ItemAddedToFold1);
					
					getCommand().isTargetVisible(title_ItemAddedToFold2);
					String title2= getCommand().getText(title_ItemAddedToFold2);
					
					String title = title1+" "+title2;
					Assert.assertEquals(title,"Your item was successfully added to My Kohler Folder", "Application is not navigated to My Kohler folder");
					
					getCommand().isTargetVisible(button_ItemAddedToFoldClose);
					getCommand().click(button_ItemAddedToFoldClose);
					
					break;
					
				case "share":
					button.click();
					getCommand().isTargetVisible(Items_shareItems);
					List<WebElement> shareLinks = getCommand().driver.findElements(By.xpath("//*[@class='share-tip organic-namespace olbox-container']/div/ul/li/a"));
					for(WebElement linkText: shareLinks)
					{
						 Share_linkText = linkText.getText();
						 log("Share options are : " + Share_linkText,LogType.STEP);
					}
					ShareLinkTextItems.add(Share_linkText);
					
					getCommand().isTargetVisible(Items_shareItemFaceebook);
					getCommand().click(Items_shareItemFaceebook);
					getCommand().waitFor(3);
					
					ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
					
					 if(browserName.equals("safari"))
						{

							getCommand().driver.switchTo().window(tabs.get(0));
						}
		    		 else
		    		 {
							getCommand().driver.switchTo().window(tabs.get(1));

		    		 }
					
					
					
					getCommand().waitFor(3);
				    log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
				    int Win_size = getCommand().driver.getWindowHandles().size();
				    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
				    
				    log("Close the Child Window: "+getCommand().driver.getTitle(),LogType.STEP);
				    getCommand().driver.close();
			    	
			    	 if(browserName.equals("safari"))
						{

					    	getCommand().driver.switchTo().window(tabs.get(1));
						}
		    		 else
		    		 {
					    	getCommand().driver.switchTo().window(tabs.get(0));

		    		 }
			    	
					
					break;
				case "print":
					break;

				default:
					throw new Exception("The type of functionality is neither of the above.");
					
				}
				
				
				}
	
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	// verify Bathroom expansion and bathroom links
	public Kohler_GeneralNavigation VerifyIdeasMainMenu()
	{
		
		try {			    
				getCommand().waitForTargetPresent(Link_Ideas);
				getCommand().click(Link_Ideas);
			
				if(getCommand().isTargetVisible(Link_ideasMainMenu))
					Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
				
				getCommand().isTargetVisible(Link_IdeasPage);
				
				int Win_size = getCommand().driver.getWindowHandles().size();
				log("Open Windows Size is :" + Win_size ,LogType.STEP);
				
				log(getCommand().getText(Link_IdeasPage),LogType.STEP);
				
				if(browserName.equals("safari"))
				{
					String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN); 				
					getCommand().sendKeys(Link_IdeasPage, selectLinkOpeninNewTab);		

				}
				else
				{
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 				
					getCommand().sendKeys(Link_IdeasPage, selectLinkOpeninNewTab);		

				}
				
			
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
				wait.until(ExpectedConditions.numberOfwindowsToBe(2));
				ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
				
				 if(browserName.equals("safari"))
					{

						getCommand().driver.switchTo().window(tabs2.get(0));
					}
	    		 else
	    		 {
	 				getCommand().driver.switchTo().window(tabs2.get(1));

	    		 }
				
				
				getCommand().waitFor(5);
			    String pageTitle = getCommand().driver.getTitle();
			    
			    Win_size = getCommand().driver.getWindowHandles().size();
			    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
			    
			    log("Page title is:" + pageTitle,LogType.STEP);
			    Assert.assertEquals(pageTitle, "The Bold Look Of Kohler", "Page is not navigated to bathroom Page");
			    		    
			    getCommand().driver.close();
			    
			    if(browserName.equals("safari"))
				{

					getCommand().driver.switchTo().window(tabs2.get(1));
				}
    		 else
    		 {
 			    getCommand().driver.switchTo().window(tabs2.get(0));

    		 }	    
			    
		
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		
		return this;
	}

	// Verify Ideas kitchen Planner link functionality+ new Window
	public Kohler_GeneralNavigation verifyIdeasKitchenPlanner()
	{
		try
		{
			log("Verify Ideas Kitchen Planner link" ,LogType.STEP);
			getCommand().waitForTargetPresent(Link_Ideas);
			getCommand().click(Link_Ideas);
		
			if(getCommand().isTargetVisible(Link_ideasMainMenu))
				Assert.assertTrue(true, "Ideas Kitchen main menu is expanded successfully");
			
			int Win_size = getCommand().driver.getWindowHandles().size();
			log("Open Windows Size is :" + Win_size ,LogType.STEP);
			
			getCommand().waitForTargetPresent(Link_IdeaskitchenPlanner);
			getCommand().click(Link_IdeaskitchenPlanner);
			WebDriverWait wait = new WebDriverWait(getCommand().driver,25);
			wait.until(ExpectedConditions.numberOfwindowsToBe(2));
			ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			
			 if(browserName.equals("safari"))
				{

					getCommand().driver.switchTo().window(tabs.get(0));
				}
 		 else
 		 {
 			getCommand().driver.switchTo().window(tabs.get(1));

 		 }

//			wait = new WebDriverWait(getCommand().driver,100);
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("footer-title")));
			getCommand().waitFor(3);
			log("Page URL is: " + getCommand().getPageUrl(),LogType.STEP);
		    Win_size = getCommand().driver.getWindowHandles().size();
		    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
		    
		    log("Close the Child Window: "+getCommand().getPageUrl(),LogType.STEP);
		    getCommand().driver.close();
		    
		    if(browserName.equals("safari"))
			{

		    	getCommand().driver.switchTo().window(tabs.get(1));
			}
		 else
		 {
		    	getCommand().driver.switchTo().window(tabs.get(0));

		 }

		
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	
		
	}
	
	// verify Bathroom menu links and link navigation
	public Kohler_GeneralNavigation verifyIdeasSubMenuLinks()
	{
		try{
			
			log("Verify Ideas sub menu items links" ,LogType.STEP);
			getCommand().waitForTargetPresent(Link_Ideas);
			getCommand().click(Link_Ideas);
		
			if(getCommand().isTargetVisible(Link_ideasMainMenu))
				Assert.assertTrue(true, "Ideas main menu is expanded successfully");
			
			List<WebElement> Ideas_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--ideas']/div/div/div"));
		    int link_count = Ideas_SubMenuLinks.size();
		    
		    for (int i = 1; i <= link_count; i++)
		    {
		    		List<WebElement> ideas_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--ideas']/div/div/div["+i+"]/ul/li"));
		    		int SubMenu_count = ideas_SubMenuLinks.size();
		    		for (int m = 1; m <= SubMenu_count; m++)
		    		{
		    			List<WebElement> Ideas_InnerLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--ideas']/div/div/div["+i+"]/ul/li["+m+"]/a"));
			    		int Links_count = Ideas_InnerLinks.size();
			    		for (int n = 0; n < Links_count; n++) 
			    		{
							
							log("verify the link text : "+  Ideas_InnerLinks.get(n).getText(),LogType.STEP);
							
							log("click on each link and verify in new tab",LogType.STEP);
							if(browserName.equals("safari"))
							{
								String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN); 
								Ideas_InnerLinks.get(n).sendKeys(selectLinkOpeninNewTab);

							}
							else
							{
								String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
								Ideas_InnerLinks.get(n).sendKeys(selectLinkOpeninNewTab);

							}
							
						
							
							
							WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
							wait.until(ExpectedConditions.numberOfwindowsToBe(2));
							ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
							
							 if(browserName.equals("safari"))
								{

									getCommand().driver.switchTo().window(tabs2.get(0));
								}
				    		 else
				    		 {
									getCommand().driver.switchTo().window(tabs2.get(1));

				    		 }

							
							getCommand().waitFor(3);
						    int Win_size = getCommand().driver.getWindowHandles().size();
						    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
						    
							String pageTitle = getCommand().driver.getTitle();
							log("verify the Page title: "+ pageTitle,LogType.STEP);
							
							getCommand().driver.close();
							log("Close the tab and switch back to parent window",LogType.STEP);
							
							 if(browserName.equals("safari"))
								{

									getCommand().driver.switchTo().window(tabs2.get(1));
								}
				    		 else
				    		 {
									getCommand().driver.switchTo().window(tabs2.get(0));

				    		 }
							
						
							
						}
					}
		    	}
		  
			
			
			
			}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// Verify HelpUsToImproveMore link functionality
	public Kohler_GeneralNavigation VerifyKitchenHelpUsToImproveMore(String text, String feedback)
	{
		log("Click on HelpusToImproveMore",LogType.STEP);
		try {
				log("verify Kitchen Main menu",LogType.STEP);
				getCommand().waitForTargetPresent(Link_Kitchen);
				getCommand().click(Link_Kitchen);
		
				if(getCommand().isTargetVisible(Link_kitchenMainMenu))
				Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
			
				getCommand().isTargetVisible(Link_KitchenPage);
				getCommand().click(Link_KitchenPage);
				
				String pageTitle = getCommand().driver.getTitle();
			    log("Page title is:" + pageTitle,LogType.STEP);
			    
			    
				log("MouseHover on HelpUsToImproveMore link",LogType.STEP);
				getCommand().scrollTo(Link_HelpUsToImproveMore);
				getCommand().waitFor(2);

				getCommand().mouseHover(Link_HelpUsToImproveMore);
				getCommand().click(Link_HelpUsToImproveMore);
				WebDriverWait wait = new WebDriverWait(getCommand().driver,50);
				wait.until(ExpectedConditions.numberOfwindowsToBe(2));
				ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
				
				
				if(browserName.equals("safari"))
				{
					getCommand().driver.switchTo().window(tabs2.get(0));

				}
				else
				{
					getCommand().driver.switchTo().window(tabs2.get(1));


				}

			
				
			    int Win_size = getCommand().driver.getWindowHandles().size();
			    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
			    getCommand().waitFor(5);
			    if(getCommand().driver.getTitle()==null)
			    {
			    	getCommand().refreshPage();
			    }
				if(browserName.equals("MicrosoftEdge"))
				{
					getCommand().driver.manage().window().maximize();
					getCommand().waitFor(5);
				}

			    wait = new WebDriverWait(getCommand().driver,50);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='submitButton']")));
			    pageTitle = getCommand().driver.getTitle();
			    log("Page title is:" + pageTitle,LogType.STEP);
			    Assert.assertEquals(pageTitle, "Kohler US Study", "Title mismatch");
			    getCommand().waitFor(5);
			    log("select the type of feedback" , LogType.STEP);
			    
			    switch (feedback) {
				case "Suggestion": 
					 	getCommand().waitForTargetVisible(btn_suggestion);
					    getCommand().click(btn_suggestion);
					    log("clicked on suggestion button" , LogType.STEP);
					break;
				case "Dislike": 
					if(browserName.equals("safari"))
					{
						getCommand().executeJavaScript("arguments[0].click()",btn_dislike);

					}
					else
					{
						getCommand().waitForTargetVisible(btn_dislike);
						   getCommand().click(btn_dislike);
					}
					
				 	
				    log("clicked on Dislike button" , LogType.STEP);
				    break;
				case "Praise": 
				 	getCommand().waitForTargetVisible(btn_praise);
				    getCommand().click(btn_praise);
				    log("clicked on praise button" , LogType.STEP);
				    break;

				default:
					break;
				}
			    
			    log("Input comments in comments section" , LogType.STEP);
			    getCommand().sendKeys(text_commenttextbox,text);
			    
			    log("Select the rating based on feedback" , LogType.STEP);
			    getCommand().waitForTargetVisible(btn_Rating);
			    getCommand().click(btn_Rating);
			    
			    log("click on sendmycomments button" , LogType.STEP);
			    getCommand().waitForTargetVisible(btn_sendmyComments);
			    getCommand().click(btn_sendmyComments);
			    getCommand().waitFor(5);
			    
			    log("click on Close the window button" , LogType.STEP);
			    getCommand().waitForTargetVisible(btn_closeTheWind);
			    getCommand().click(btn_closeTheWind);
			    
			    log("Switch to main Window" , LogType.STEP);
			    
			    if(browserName.equals("safari"))
				{

				    getCommand().driver.switchTo().window(tabs2.get(1));
				}
    		 else
    		 {
 			    getCommand().driver.switchTo().window(tabs2.get(0));

    		 }

			
			    
			    Assert.assertEquals(getCommand().driver.getTitle() ,"Kitchen | KOHLER", "Home Page Title mismatch");
			    	
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// Verify Newsletter Sign Up link functionality
	public Kohler_GeneralNavigation VerifyNewsLetterSignUpLink(String testData)
		{
			NewsLetterSignUp newsLetterSignUp = NewsLetterSignUp.fetch(testData);
			
			try {
					String emailID = newsLetterSignUp.EmailId;
					String firstName = newsLetterSignUp.FirstName;
					String lastName = newsLetterSignUp.LastName;
					String postalCode = newsLetterSignUp.PostCode;
					String country = newsLetterSignUp.Country;
					String occupation = newsLetterSignUp.Occupation;
					
					log("verify Newsletter Sign Up link",LogType.STEP);
					getCommand().isTargetVisible(link_NewsLetterSignUp);
					getCommand().click(link_NewsLetterSignUp);
					
					WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
					wait.until(ExpectedConditions.numberOfwindowsToBe(2));
					ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
					
					 if(browserName.equals("safari"))
						{

							getCommand().driver.switchTo().window(tabs2.get(0));
						}
		    		 else
		    		 {
							getCommand().driver.switchTo().window(tabs2.get(1));

		    		 }
					
					
					int Win_size = getCommand().driver.getWindowHandles().size();
				    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
				    
				    wait = new WebDriverWait(getCommand().driver,10);
					wait.until(ExpectedConditions.titleContains(""));
					String pageTitle = getCommand().driver.getTitle();
					log("verify the Page title"+ pageTitle,LogType.STEP);
					
					log("Input the values to subscribe to News letter",LogType.STEP);
					getCommand().isTargetVisible(text_Email);
					getCommand().sendKeys(text_Email, emailID);
					getCommand().isTargetVisible(text_FirstName);
					getCommand().sendKeys(text_FirstName, firstName);
					getCommand().isTargetVisible(text_LastName);
					getCommand().sendKeys(text_LastName, lastName);
					
					getCommand().isTargetVisible(text_PostalCode);
					getCommand().sendKeys(text_PostalCode, postalCode);
					
					getCommand().isTargetVisible(select_Country);
					getCommand().selectDropDown(select_Country, Integer.parseInt(country));
					
					getCommand().isTargetVisible(select_occupation);
					getCommand().selectDropDown(select_occupation, Integer.parseInt(occupation));
					
					log("Click on newletter that you want to receive",LogType.STEP);
					//getCommand().waitForTargetVisible(button_interiorProducts);
					//getCommand().isTargetVisible(button_interiorProducts);
					if(browserName.equals("safari"))
					{
						WebElement btn_Interior=getCommand().driver.findElement(By.xpath("//*[@id=\"tab-2\"]"));
					//WebElement btn_Interior=getCommand().driver.findElement(By.xpath("//*[@id='heading2']//following::a"));
					js.executeScript("arguments[0].click();", btn_Interior);
					}
					else
					{
					getCommand().click(button_interiorProducts);
					}
					
					if(getCommand().isTargetVisible(Div_ExpandedFields))
						Assert.assertTrue(true, "Div is expanded successfully");
					
					//getCommand().isTargetVisible(checkbox_interiorProducts);
					//getCommand().click(checkbox_interiorProducts);
					
					getCommand().isTargetVisible(button_Submit);
					getCommand().click(button_Submit);
					
					getCommand().waitFor(5);
					
//					String ThankyouText=getCommand().driver.getPageSource();
					//System.out.println(ThankyouText);
//					if(ThankyouText.trim().contains("Thank you for your time. Please go to"))
//					{
//						log("Thank you message is displayed",LogType.STEP);
//					}
//					else
//					{
//						log("Thank you message is not displayed",LogType.STEP);
//						Assert.fail();
//					}
					
					//System.out.println(thankyouText);
					getCommand().waitForTargetPresent(text_thankyou);
					
					//getCommand().isTargetPresentAfterWait(text_thankyou, 15);
				
					System.out.println(getCommand().getText(text_thankyou));
					log("On clicking Submit button: "+ getCommand().getText(text_thankyou),LogType.STEP);
					
					getCommand().driver.close();
					log("Close the tab and switch back to parent window",LogType.STEP);
					
					 if(browserName.equals("safari"))
						{

							getCommand().driver.switchTo().window(tabs2.get(1));
						}
		    		 else
		    		 {
							getCommand().driver.switchTo().window(tabs2.get(0));

		    		 }

				
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			
			return this;
		}


	// verify Parts expansion and Parts links
	public Kohler_GeneralNavigation VerifyPartsMainMenu()
	{
			
			try {		
				
						String text=null;
						log("verify Parts Main menu",LogType.STEP);
						getCommand().waitForTargetPresent(Link_Parts);
						getCommand().click(Link_Parts);
					
						if(getCommand().isTargetVisible(Link_PartsMainMenu))
							Assert.assertTrue(true, "Parts main menu is expanded successfully");
						
						log("Click on Parts Link",LogType.STEP);
						getCommand().isTargetVisible(Link_PartsPage);
						getCommand().click(Link_PartsPage);
						getCommand().waitFor(2);
						
						log("On clicking on Parts link, Parts Page should be displayed",LogType.STEP);
						Assert.assertEquals(getCommand().driver.getTitle(), "Maintenance & Replacement Parts | KOHLER", "Not landed in Parts maintainence Page");
						
						
						log("click on Wizard",LogType.STEP);
						getCommand().waitForTargetVisible(Link_PartsWizard);
						getCommand().isTargetVisible(Link_PartsWizard);
						getCommand().click(Link_PartsWizard);
						getCommand().waitFor(3);
						
						List<WebElement> divs = getCommand().driver.findElements(By.xpath("//div[@class='product-finder']/div"));
						
						for (int i = 1; i < divs.size(); i++)
						{
							text = getCommand().driver.findElement(By.xpath("//div[@class='product-finder']/div["+i+"]/div/h3")).getText();
							log("get the text of Parts: "+ text ,LogType.STEP);
							getCommand().waitFor(5);
							if(text.contains("ROOM") || text.contains("Room"))
							{
								getCommand().driver.findElement(By.xpath("//*[@id='question-room']/div[2]/ul/li[1]")).click();
								
								boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-room']/div[1]/figure")).isDisplayed();
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								getCommand().waitFor(10);
							}
							else if (text.contains("PRODUCT") || text.contains("Product"))
							{
								getCommand().driver.findElement(By.xpath("//*[@id='question-product-type']/div[2]/ul/li[1]")).click();
								boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-product-type']/div[1]/figure")).isDisplayed();
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								getCommand().waitFor(10);
							}
								
							else if (text.contains("FEATURE") || text.contains("Features"))
							{
								getCommand().driver.findElement(By.xpath("//*[@id='feature-type']/ul/li[1]")).click();
								boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-features']/div[1]/figure")).isDisplayed();
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								getCommand().waitFor(10);
							}
							
						}
							
							log("verify the count on results button : ",LogType.STEP);
							getCommand().waitForTargetVisible(button_ViewResultsCount);
							getCommand().isTargetVisible(button_ViewResultsCount);
							String countIs = getCommand().getText(button_ViewResultsCount);
							
							
							getCommand().isTargetVisible(button_ViewResults);
							getCommand().click(button_ViewResults);
							
							getCommand().isTargetVisible(Count_Results);
							String count = getCommand().getText(Count_Results);
							String TotalCountIs = "";
							TotalCountIs = count.substring(0, 3);
							
							Assert.assertEquals(countIs, TotalCountIs,"Results are not the same");

				}catch(Exception ex)
				{
					Assert.fail(ex.getMessage());
				}
				
				
				return this;
			}

	// verify Find a Pro Functionality with invalid Zip code
	public Kohler_GeneralNavigation VerifyFindAProInvalidZipCode(String Testdata)
	{
		SearchData search = SearchData.fetch(Testdata);
		try {		
			
			String value = search.keyword; 
			ArrayList<String> elements_value = new ArrayList<String>();
			log("verify Find a pro functionality",LogType.STEP);
			getCommand().waitForTargetPresent(link_FindAPro);
			getCommand().isTargetVisible(link_FindAPro);
			
			
			getCommand().click(link_FindAPro);
			getCommand().waitForTargetVisible(Header);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver,120);
			wait.until(ExpectedConditions.titleContains("Find a Pro | KOHLER"));
			
			String title = getCommand().driver.getTitle();
			Assert.assertEquals(title, "Find a Pro | KOHLER");
			
			Assert.assertTrue(getCommand().isTargetVisible(Ele_NeedHelpWith), "Element is not displayed in Find a Pro Page");
			
			Assert.assertTrue(getCommand().isTargetVisible(Ele_CustomerSupport),"Element is not displayed in Find a Pro Page");
			
			getCommand().isTargetVisible(EleValue_NeedHelpWith);
			String val = getCommand().getText(EleValue_NeedHelpWith);
			
			String expect = "I NEED HELP WITH";
			if(val.trim().equalsIgnoreCase(expect))
			{
				List<WebElement> inner_Ele = getCommand().driver.findElements(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']/button/header/h3"));
				int count = inner_Ele.size();
				
				for (int i = 0; i < count; i++)
				{
					log("verify element text in Need help : "+ inner_Ele.get(i).getText(),LogType.STEP);
					elements_value.add(inner_Ele.get(i).getText());
				}
				
				
				for (int i = 0; i < count; i++)
				{
					inner_Ele = getCommand().driver.findElements(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']/button/header/h3"));
					
					if(!inner_Ele.get(i).getText().trim().equalsIgnoreCase("BATHROOM DESIGN HELP"))
					{
						int x = i+1;


						getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']["+x+"]/button/header/h3")).click();
						if(x==3 || x==4)
							x= x-1;
						getCommand().driver.findElement(By.xpath("//label[@class='input__label']//following::input[@class='input__value']["+x+"]")).sendKeys(value);
						getCommand().driver.findElement(By.xpath("//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']["+x+"]")).click();
						getCommand().waitFor(2);
						log("verify the landing page Title : " +getCommand().driver.getTitle() ,LogType.STEP);
						getCommand().isTargetVisible(text_FindAProErrorMsg);
						
						getCommand().waitFor(5);
						log("verify the text msg in landing page " + getCommand().getText(text_FindAProErrorMsg),LogType.STEP);
						if(getCommand().getText(text_FindAProErrorMsg).contains("Sorry. We have no professional partners for the zip code you entered."))
							
						Assert.assertTrue(true, "We are not in related page");
						getCommand().waitFor(5);			
						getCommand().isTargetVisible(Link_GoBack);
						
						if(browserName.equals("safari"))
						{
							getCommand().driver.navigate().back();
							getCommand().waitFor(2);
						}
						else
						{
						getCommand().click(Link_GoBack);
						
						}
						

						wait = new WebDriverWait(getCommand().driver,25);
						wait.until(ExpectedConditions.titleContains("Find a Pro | KOHLER"));
						
					}
					else
					{
						if(browserName.equals("safari"))
						{
							js.executeScript("arguments[0].click();", inner_Ele.get(i));

						}
						else
						{
						inner_Ele.get(i).click();
						}

						getCommand().isTargetVisible(button_getDesignHelp);
						getCommand().click(button_getDesignHelp);
						getCommand().waitForTargetVisible(Logo);
												
						log("verify landing page title : "+ getCommand().driver.getTitle(),LogType.STEP);
						
						getCommand().driver.navigate().back();
						wait = new WebDriverWait(getCommand().driver,25);
						wait.until(ExpectedConditions.titleContains("Find a Pro | KOHLER"));
					}
					
					
				}
				
				log("verify landing page of customer support",LogType.STEP);
				getCommand().isTargetVisible(button_CustSupport);
				getCommand().click(button_CustSupport);
				getCommand().waitFor(3);
				log("verify landing page title : "+ getCommand().driver.getTitle(),LogType.STEP);
				getCommand().driver.navigate().back();
				wait = new WebDriverWait(getCommand().driver,25);
				wait.until(ExpectedConditions.titleContains("Find a Pro | KOHLER"));
			}


			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
	
	
	return this;
	}
	
	// verify Find a Pro Functionality with valid Zip code
	public Kohler_GeneralNavigation VerifyFindAPro(String ProductInst , String BathHome)
	{
		
		SearchData SearchData1 = SearchData.fetch(ProductInst);
		SearchData SearchData2 = SearchData.fetch(BathHome);
		try {		
			
			String ProductInstvalue = SearchData1.keyword; 
			String BathRoomHomeCons = SearchData2.keyword;
			
			log("verify Find a pro functionality",LogType.STEP);
			getCommand().isTargetVisible(link_FindAPro);
			getCommand().click(link_FindAPro);
			getCommand().waitFor(5);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
			wait.until(ExpectedConditions.titleContains("Find a Pro | KOHLER"));
			
			String title = getCommand().driver.getTitle();
			Assert.assertEquals(title, "Find a Pro | KOHLER");
			
			Assert.assertTrue(getCommand().isTargetVisible(Ele_NeedHelpWith), "Element is not displayed in Find a Pro Page");
			
			Assert.assertTrue(getCommand().isTargetVisible(Ele_CustomerSupport),"Element is not displayed in Find a Pro Page");
			
			List<WebElement> inner_Ele = getCommand().driver.findElements(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']/button/header/h3"));
			int count = inner_Ele.size();
					
			for (int i = 0; i < count; i++)
			{
					inner_Ele = getCommand().driver.findElements(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']/button/header/h3"));
					if(inner_Ele.get(i).getText().trim().equalsIgnoreCase("KOHLER PRODUCT INSTALLATION"))
					{
						int x = i+1;
						getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']["+x+"]/button/header/h3")).click();
						getCommand().driver.findElement(By.xpath("//label[@class='input__label']//following::input[@class='input__value']["+x+"]")).sendKeys(ProductInstvalue);
						getCommand().driver.findElement(By.xpath("//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']["+x+"]")).click();
						
						log("verify the landing page Title : " +getCommand().driver.getTitle() ,LogType.STEP);
						getCommand().isTargetVisible(text_FAPPageHeader);
						
						log("verify the text msg in landing page " + getCommand().getText(text_FAPPageHeader),LogType.STEP);
						
						Assert.assertTrue(getCommand().isTargetPresent(button_GetAnEstm),"Get an Estimation button is not visible");
						
						log("verify that view result page is displayed with address",LogType.STEP);
						if(getCommand().isTargetVisible(result_Address) && getCommand().getTargetCount(result_Address)> 0)
							Assert.assertTrue(true, "Search results are available and visible");
						
						log("verify the Get an estimation button" + getCommand().getText(text_FAPPageHeader),LogType.STEP);
						getCommand().isTargetVisible(button_GetAnEstm);
						getCommand().click(button_GetAnEstm);
						getCommand().waitForTargetPresent(text_getAnEstmHeader);
						getCommand().waitFor(3);
						
						log("verify landing page title : "+ getCommand().driver.getTitle(),LogType.STEP);
						
						Assert.assertEquals(getCommand().getText(text_getAnEstmHeader), "Get an Estimate", "Header message mismatches");
						
						log("Click on Go back link and it should navigate user to previous page",LogType.STEP);
						getCommand().isTargetVisible(Link_GoBack);
						
						getCommand().click(Link_GoBack);
						wait = new WebDriverWait(getCommand().driver,25);
						wait.until(ExpectedConditions.titleContains("Results | Find a Pro | KOHLER"));
						
						log("Click on back button and it should navigate user to previous page"+ getCommand().driver.getTitle(),LogType.STEP);
						//
						String browserName = caps.getBrowserName();
						if(browserName.equals("firefox"))
						{
							getCommand().isTargetVisible(Link_GoBack);
							getCommand().click(Link_GoBack);
						}

						else
						{
							getCommand().driver.navigate().back();
						}

						
						wait = new WebDriverWait(getCommand().driver,25);
						wait.until(ExpectedConditions.titleContains("Find a Pro | KOHLER"));
						
			
						
					}
					else if(inner_Ele.get(i).getText().trim().equalsIgnoreCase("FULL BATHROOM REMODEL"))
					{
						int x = i+1;
						getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']["+x+"]/button/header/h3")).click();
						if(x==3)
							x= x-1;
						getCommand().driver.findElement(By.xpath("//label[@class='input__label']//following::input[@class='input__value']["+x+"]")).sendKeys(BathRoomHomeCons);
						getCommand().driver.findElement(By.xpath("//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']["+x+"]")).click();
						
						log("verify the landing page Title : " +getCommand().driver.getTitle() ,LogType.STEP);
						getCommand().isTargetVisible(text_FAPPageHeader);
						
						log("verify the text msg in landing page " + getCommand().getText(text_FAPPageHeader),LogType.STEP);
						Assert.assertTrue(getCommand().isTargetPresent(button_GetAnEstm),"Get an Estimation button is not visible");
						
						if(getCommand().isTargetVisible(result_Address) && getCommand().getTargetCount(result_Address)> 0)
							Assert.assertTrue(true, "Search results are available and visible");
												
						getCommand().isTargetVisible(button_GetAnEstm);
						getCommand().click(button_GetAnEstm);
						getCommand().waitFor(5);
						
						log("verify landing page title : "+ getCommand().driver.getTitle(),LogType.STEP);
												
						Assert.assertEquals(getCommand().getText(text_getAnEstmHeader), "Get an Estimate", "Header message mismatches");
						
						log("Click on Go back link and it should navigate user to previous page",LogType.STEP);
						getCommand().isTargetVisible(Link_GoBack);
						getCommand().click(Link_GoBack);
						
						wait = new WebDriverWait(getCommand().driver,25);
						wait.until(ExpectedConditions.titleContains("Results | Find a Pro | KOHLER"));
						
						log("Click on back button and it should navigate user to previous page"+ getCommand().driver.getTitle(),LogType.STEP);
						String browserName = caps.getBrowserName();
						if(browserName.equals("firefox"))
						{
							getCommand().isTargetVisible(Link_GoBack);
							getCommand().click(Link_GoBack);
						}

						else
						{
							getCommand().driver.navigate().back();
						}
						wait = new WebDriverWait(getCommand().driver,25);
						wait.until(ExpectedConditions.titleContains("Find a Pro | KOHLER"));
						
					}
					else if(inner_Ele.get(i).getText().trim().equalsIgnoreCase("NEW HOME CONSTRUCTION"))
					{
						int x = i+1;
						getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']["+x+"]/button/header/h3")).click();
						if(x==4)
							x= x-1;
						getCommand().driver.findElement(By.xpath("//label[@class='input__label']//following::input[@class='input__value']["+x+"]")).sendKeys(BathRoomHomeCons);
						getCommand().driver.findElement(By.xpath("//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']["+x+"]")).click();
						
						log("verify the landing page Title : " +getCommand().driver.getTitle() ,LogType.STEP);
						getCommand().isTargetVisible(text_FAPPageHeader);
						
						log("verify the text msg in landing page " + getCommand().getText(text_FAPPageHeader),LogType.STEP);
						
						if(getCommand().isTargetVisible(result_Address) && getCommand().getTargetCount(result_Address)> 0)
							Assert.assertTrue(true, "Search results are available and visible");
												
						if(!getCommand().isTargetVisible(button_GetAnEstm));
						Assert.assertTrue(true, "Get an Estimation Button is not visible");
						
						log("Click on Go back link and it should navigate user to previous page",LogType.STEP);
						getCommand().isTargetVisible(Link_GoBack);
						getCommand().click(Link_GoBack);
						wait = new WebDriverWait(getCommand().driver,25);
						wait.until(ExpectedConditions.titleContains("Find a Pro | KOHLER"));
						
					}
				}
				
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
	
	
	return this;
	}

	
	// verify Kitchen menu links navigation and Page Title
	public Kohler_GeneralNavigation verifyKitchenSubMenuLinksPageTitles()
		{
			ArrayList<String> Pagetitles = new ArrayList<String>();
			try{
				
				getCommand().waitForTargetPresent(Link_Kitchen);
				getCommand().click(Link_Kitchen);
			
				if(getCommand().isTargetVisible(Link_kitchenMainMenu))
					Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
				
				List<WebElement> Kitchen_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div"));
			    int link_count = Kitchen_SubMenuLinks.size();
			    
			    for (int i = 1; i <= link_count; i++)
			    {
			    		List<WebElement> Kitchen_Links = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div["+i+"]/ul/li"));
			    		int Links_count = Kitchen_Links.size();
			    		for (int m = 1; m <= Links_count; m++)
			    		{
			    			List<WebElement> Kitchen_InnerLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div["+i+"]/ul/li["+m+"]/a"));
				    		int InnerLinks_count = Kitchen_InnerLinks.size();
				    		for (int n = 0; n < InnerLinks_count; n++) 
				    		{
								log("verify the link text"+  Kitchen_InnerLinks.get(n).getText(),LogType.STEP);
								
								log("click on each link and verify in new tab",LogType.STEP);
								
								if(browserName.equals("safari"))
								{
									String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN); 
									Kitchen_InnerLinks.get(n).sendKeys(selectLinkOpeninNewTab);

								}
								else
								{
									String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
									Kitchen_InnerLinks.get(n).sendKeys(selectLinkOpeninNewTab);

								}
								
								
								
								WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
								wait.until(ExpectedConditions.numberOfwindowsToBe(2));
								ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
								
								 if(browserName.equals("safari"))
									{

										getCommand().driver.switchTo().window(tabs2.get(0));
									}
					    		 else
					    		 {
										getCommand().driver.switchTo().window(tabs2.get(1));

					    		 }

								
								getCommand().waitFor(2);														
								int Win_size = getCommand().driver.getWindowHandles().size();
								Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
							    
								String pageTitle = getCommand().driver.getTitle();
								log("verify the Page title"+ pageTitle,LogType.STEP);
								Pagetitles.add(pageTitle);
												
								getCommand().driver.close();
								log("Close the tab and switch back to parent window",LogType.STEP);
								 if(browserName.equals("safari"))
									{

										getCommand().driver.switchTo().window(tabs2.get(1));
									}
					    		 else
					    		 {
										getCommand().driver.switchTo().window(tabs2.get(0));

					    		 }
								
							
							}
						}
			    	
			    		
			    }
				
			    int title_count = Pagetitles.size();
				
				for (int j = 0; j < title_count; j++)
				{
					getCommand().waitFor(5);
					String titleIs = Pagetitles.get(j);
					String TitleContains ="";
					if(titleIs.length()> 6)
					{
						TitleContains = titleIs.substring(titleIs.length()- 6);
						if(browserName.equals("safari"))
						{
							if(titleIs.trim().contains("KOHLER") && TitleContains.length() > 0)

									{
										log("Page title contains is : "+TitleContains,LogType.STEP);
									}
									else
									{	log("Page title is not ending with KOHLER" +TitleContains,LogType.ERROR_MESSAGE);
										Assert.fail("Page title is not ending with KOHLER" + TitleContains);
									}
						}
						
						else
						{
						if(TitleContains.contains("KOHLER") && TitleContains.length() > 0)
						{
							log("Page title contains is : "+TitleContains,LogType.STEP);
						}
						else
						{	log("Page title is not ending with KOHLER" +TitleContains,LogType.ERROR_MESSAGE);
							Assert.fail("Page title is not ending with KOHLER" + TitleContains);
						}
					}
					}
					
				}
				
				
				}
			
			catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			
			return this;
		}

	
		// polling wait for tab verification
		public void GetPageTitle(String element) throws InterruptedException
		    {
		        String ele;        
		        int retryCount = 0;
		        
		        // Get Document from DocumentDb passing in Database, Collection, and the query to fetch the document.
		        do
		        {
		            ele  = element;
		            retryCount++;
		            if (ele == null)
		            	getCommand().waitFor(2);
		        } while (retryCount < 4 && ele == null);
		        Assert.assertNotNull(ele, "Failed to fetch the element");
		        
		    }
		
		// polling wait for tab verification
				public void GetOpenWindowCount(int count ) throws InterruptedException
				    {
				        int ele;        
				        int retryCount = 0;
				        
				        // Get Document from DocumentDb passing in Database, Collection, and the query to fetch the document.
				        do
				        {
				            ele  = count;
				            retryCount++;
				            if (ele == 0)
				            	getCommand().waitFor(2);
				        } while (retryCount < 4 && ele == 0);
				        Assert.assertNotNull(ele, "Failed to fetch the element");
				        
				    }

	
	
	
	
}

