package com.components.pages;

import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.components.repository.SiteRepository;
import com.components.yaml.DTVData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;

public class Kohler_DTVPage extends SitePage {
	
		
	/* Defining the locators on the Page */
	public static final Target dtv_Title = new Target("dtv_Title","/html/head/title", Target.XPATH);
	public static final Target gn_Bathroom = new Target("gn_bathroom","//*[@id=\"main-nav__button--bathroom\"]",Target.XPATH);
	public static final Target gn_sub_ExploreAll = new Target("gn_sub_ExploreAll","//*[@id=\"sub-nav-tab--bathroom\"]/div/a/span[2]",Target.XPATH);
	public static final Target car_DTV = new Target("car_DTV","//*[@id=\"section__carousel-hero\"]/div/div/div[2]/div/div/div/div/div/h2", Target.XPATH);
	public static final Target share_DTV = new Target("share_DTV","//*[@id=\"share-buttons-trigger\"]/img",Target.XPATH);
	public static final Target car_nav_DTV = new Target("car_nav_DTV","//*[@id=\"section__carousel-hero\"]/ul/li[1]/button", Target.XPATH);
	public static final Target share_fb_DTV = new Target("share_fb_DTV","//*[@id=\"shareFacebook\"]/a/img", Target.XPATH);
	public static final Target share_pin_DTV = new Target("share_pin_DTV","//*[@id=\"sharePinterest\"]/a/img", Target.XPATH);
	public static final Target share_houzz_DTV = new Target("share_houzz_DTV","//*[@id=\"shareHouzz\"]/img", Target.XPATH);
	public static final Target img_DTV = new Target("img_DTV","//*[@id=\"dtv-promo\"]/div[1]/h2/img",Target.XPATH);
	public static final Target discover_CTA_DTV = new Target("discover_CTA_DTV","//*[@id=\"dtv-promo\"]/div[1]/p[2]/a",Target.XPATH);
	public static final Target discover_CTA_DTV_ff = new Target("discover_CTA_DTV_ff","//*[@class=\"cta-button\"]",Target.XPATH);
	public static final Target enter_zip_DTV = new Target("enter_zip_DTV","//*[@id=\"dtv-promo-form\"]/input[2]",Target.XPATH);
	public static final Target enter_zip_DTV_ff = new Target("enter_zip_DTV_ff","//*[@class=\"validate-zip-or-zip-plus4-or-canadian-postal-code\"]",Target.XPATH);
	public static final Target btn_store_locator_DTV = new Target("btn_store_locator_DTV","//*[@id=\"dtv-promo-form\"]/button",Target.XPATH);
	public static final Target btn_store_locator_DTV_ff = new Target("btn_store_locator_DTV_ff","//*[@class=\"content\"]/form/button",Target.XPATH);
	public static final Target scrubber_div_DTV = new Target("scrubber_DTV","//*[@id=\"dtv-details-content\"]",Target.XPATH);
	public static final Target scrubber_restart_DTV = new Target("scrubber_Restart_DTV","//*[@id=\"detailsScrubberReset\"]/span",Target.XPATH);
	public static final Target scrubber_DTV = new Target("scrubber_DTV","//*[@id=\"detailsScrubber\"]",Target.XPATH);
	public static final Target btn_prev_shower_DTV = new Target("btn_prev_shower_DTV","//*[@id=\"dtv-shower-btnPrev\"]",Target.XPATH);
	public static final Target btn_next_shower_DTV = new Target("btn_next_shower_DTV","//*[@id=\"dtv-shower-btnNext\"]",Target.XPATH);
	public static final Target div_shower_bgs = new Target("div_shower_bgs","//*[@id=\"shower-bgs\"]",Target.XPATH);
	public static final Target div_promo = new Target("div_promo","//*[@id=\"dtv-promo\"]/div[2]",Target.XPATH);	
	
	DTVData dtvData = DTVData.fetch("DTVData");
	JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
	
	public Kohler_DTVPage (SiteRepository repository)
	{
		super(repository);
	}
	
	public Kohler_DTVPage atDTVPage()
	{
		try 
		{
			getCommand().waitForTargetPresent(gn_Bathroom);
			getCommand().waitForTargetPresent(gn_Bathroom).click(gn_Bathroom);
			getCommand().waitForTargetPresent(gn_sub_ExploreAll).click(gn_sub_ExploreAll);
			getCommand().waitForTargetPresent(car_DTV).mouseHover(car_DTV);
			getCommand().click(car_nav_DTV);
			js.executeScript("window.scrollBy(0,-200)");
			//getCommand().click(car_nav_DTV);
			getCommand().click(car_DTV);
		} 
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		log("DTV page opened",LogType.STEP);
		return this;
	}
	

		//Verify DTV Page Load
		public Kohler_DTVPage verifyDTVPageLoad()
		{
			try 
			{
				//Retrieving page state
				JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
				String test = js.executeScript("return document.readyState").toString();
				
				//Added for edge
				while (!test.equalsIgnoreCase("complete"))
				{
					getCommand().waitFor(1);
					test = js.executeScript("return document.readyState").toString();
				}
				
				//Verifying page load
				getCommand().waitFor(5);
				if (test.equalsIgnoreCase("complete")) {
					Assert.assertEquals(getCommand().driver.getTitle(), dtvData.PageTitle);
					log ("DTV Page loaded",LogType.STEP);
				}
				else
				{
					log ("DTV Page not loaded",LogType.ERROR_MESSAGE);
				}
			}
			catch (Exception ex) {
				Assert.fail(ex.getMessage());
			}
			return this;
		}
	
	//Verify DTV Page Title
	public Kohler_DTVPage verifyDTVTitle()
	{
		pageLoad();
		getCommand().waitFor(5);
		try 
		{
			String pageTitle = getCommand().driver.getTitle();
			Assert.assertEquals(pageTitle, dtvData.PageTitle);
			log ("DTV Page Title Verified",LogType.STEP);
		} 
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Share Section on DTV Page
	public Kohler_DTVPage verifyDTVShare()
	{
		try 
		{
			pageLoad();
			getCommand().waitFor(5);
			//Finding Share section and its opacity
			WebElement share_btns = getCommand().driver.findElement(By.xpath("//*[@id=\"shareButtons\"]"));
			String opacity = share_btns.getCssValue("opacity");
			scrollDown();
			
			//Share section activation and assertion
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);
			js.executeScript("window.scrollBy(0,-90)");
			getCommand().waitForTargetPresent(share_DTV).click(share_DTV);
			getCommand().waitFor(2);
			Assert.assertNotEquals(share_btns.getCssValue("opacity"), opacity);
			log ("Share buttons activated", LogType.STEP);
			
			//Verifying share icons
    		WebElement share_section = getCommand().driver.findElement(By.xpath("//*[@id=\"shareButtons\"]"));
    		List<WebElement> share_links = share_section.findElements(By.tagName("li"));
    		
    		for (int i=0; i<share_links.size();i++)
    		{
    			switch(share_links.get(i).getAttribute("id"))
    			{
    			case "shareHouzz":
    				share_links.get(i).click();
    				share_links.get(i).click();
   				
    				log("Share on Houzz activated",LogType.STEP);
    				getCommand().waitFor(4);
    				ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
    				getCommand().waitFor(1);
    				log("Switching to new tab",LogType.STEP);
    				getCommand().driver.switchTo().window(listofTabs.get(0));
    				Assert.assertEquals(getCommand().driver.getTitle(),"Sign In");
    				log("Houzz page verified",LogType.STEP);
    				getCommand().driver.close();
    				getCommand().driver.switchTo().window(listofTabs.get(1)); 
    				log("Switching back to original tab",LogType.STEP);
    				break;
    				
//    			case "sharePinterest":
//    				share_links.get(i).click();
//    				log("Share on Pinterest activated",LogType.STEP);
//    				getCommand().waitFor(4);
//    				ArrayList<String> listofTabs1 = new ArrayList<String> (getCommand().driver.getWindowHandles());
//    				getCommand().waitFor(1);
//                    log("Switching to new tab",LogType.STEP);
//                    getCommand().driver.switchTo().window(listofTabs1.get(1));
//                    Assert.assertEquals(getCommand().driver.getTitle(),"www.pinterest.com");
//                    log("Pinterest page verified",LogType.STEP);
//                    getCommand().driver.close();
//                    getCommand().driver.switchTo().window(listofTabs1.get(0)); 
//                    log("Switching back to original tab",LogType.STEP);
//    				break;
    				
    			case "shareFacebook":
    				share_links.get(i).click();
    				log("Share on Facebook activated",LogType.STEP);
    				getCommand().waitFor(4);
    				ArrayList<String> listofTabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
    				getCommand().waitFor(1);
                    log("Switching to new tab",LogType.STEP);
                    getCommand().driver.switchTo().window(listofTabs2.get(1));
                    getCommand().waitFor(1);
                    Assert.assertEquals(getCommand().driver.getTitle(),"Facebook");
                    log("Facebook page verified",LogType.STEP);
                    getCommand().driver.close();
                    getCommand().driver.switchTo().window(listofTabs2.get(0)); 
                    log("Switching back to original tab",LogType.STEP);
    				break;
    			}
    		}			
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	//Verify WTB section on DTV Page
	public Kohler_DTVPage verifyDTVWTB()
	{
		pageLoad();
		try 
		{
			//scrolling to CTA
			scrollDown();
			getCommand().waitFor(2);
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", div_promo);
			//Verify CTA opens PDP page
			log("DTV+ PDP tab opened",LogType.STEP);
			
			Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
			String browserName = caps.getBrowserName();
			
			log("Switching to DTV+ PDP tab",LogType.STEP);
			getCommand().waitFor(2);
			if(browserName.equals("firefox"))
			{
				getCommand().sendKeys(discover_CTA_DTV_ff, Keys.chord(Keys.CONTROL,Keys.RETURN));
				getCommand().waitFor(4);
				ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
				getCommand().waitFor(1);
	            getCommand().driver.switchTo().window(listofTabs.get(1));
	            pageLoad();
	            boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/productDetail/");
	            Assert.assertEquals(true, status_PDP);
	            Assert.assertEquals(getCommand().driver.getTitle(),dtvData.WTBProduct_PageTitle);
	            log("DTV+ PDP tab verified",LogType.STEP);
	            getCommand().driver.close();
	            getCommand().driver.switchTo().window(listofTabs.get(0));
	            log("Switching back to original tab",LogType.STEP);
	            
	            //Verify enter ZIP and Store Locator page load
	    		DTVData dtvData = DTVData.fetch("DTVData");
	    		log("Entering ZipCode and Finding Showroom",LogType.STEP);
	    		
	    		JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
				js.executeScript("document.getElementsByName('postalCode')[0].value='94538'");
	    		
	            getCommand().sendKeys(btn_store_locator_DTV_ff, Keys.chord(Keys.CONTROL,Keys.RETURN));

	            log("Opening Store Locator",LogType.STEP);
	            pageLoad();
	            getCommand().waitFor(5);
	            boolean status_Locator = getCommand().driver.getCurrentUrl().contains("/storelocator/");
	            Assert.assertEquals(true, status_Locator);
	            Assert.assertEquals(getCommand().driver.getTitle(),dtvData.StoreFinder_PageTitle);
	            log("Store Locator verified",LogType.STEP);
			}
			else
			{
				getCommand().sendKeys(discover_CTA_DTV, Keys.chord(Keys.COMMAND,Keys.RETURN));
				getCommand().waitFor(5);
				ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
				
				if(browserName.equals("MicrosoftEdge") && listofTabs.size() == 1)
				{
					boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/productDetail/");
		            Assert.assertEquals(true, status_PDP);
		            Assert.assertEquals(getCommand().driver.getTitle(),dtvData.WTBProduct_PageTitle);
		            log("DTV+ PDP tab verified",LogType.STEP);
		            getCommand().driver.navigate().back();
		            scrollDown();
					getCommand().waitFor(2);
					getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);
					getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", div_promo);
				}
				else
				{
					getCommand().driver.switchTo().window(listofTabs.get(0));
					pageLoad();
					getCommand().waitFor(2);
					boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/productDetail/");
					Assert.assertEquals(true, status_PDP);
					Assert.assertEquals(getCommand().driver.getTitle(),dtvData.WTBProduct_PageTitle);
					log("DTV+ PDP tab verified",LogType.STEP);
					getCommand().driver.close();
					getCommand().driver.switchTo().window(listofTabs.get(1));
				}
	            log("Switching back to original tab",LogType.STEP);
				
	            //Verify enter ZIP and Store Locator page load
	    		DTVData dtvData = DTVData.fetch("DTVData");
	    		log("Entering ZipCode and Finding Showroom",LogType.STEP);  
	    		
	    		
	           getCommand().sendKeys(enter_zip_DTV, dtvData.ZipCode);
	            getCommand().sendKeys(btn_store_locator_DTV, Keys.chord(Keys.COMMAND,Keys.RETURN));
	            
	            if(browserName.equals("chrome"))
	            {
	            	ArrayList<String> listofTabs1 = new ArrayList<String> (getCommand().driver.getWindowHandles());
		            log("Switching to Store Locator tab",LogType.STEP);
		            getCommand().driver.switchTo().window(listofTabs1.get(1));
		            pageLoad();
		            boolean status_Locator = getCommand().driver.getCurrentUrl().contains("/storelocator/");
		            Assert.assertEquals(true, status_Locator);
		            Assert.assertEquals(getCommand().driver.getTitle(),dtvData.StoreFinder_PageTitle);
		            log("Store Locator tab verified",LogType.STEP);
		            getCommand().driver.close();
		            getCommand().driver.switchTo().window(listofTabs1.get(0));
		            log("Switching back to original tab",LogType.STEP);
	            }
	            else
	            {
	            	log("Navigating to Store Locator tab",LogType.STEP);
	            	pageLoad();
	            	boolean status_Locator = getCommand().driver.getCurrentUrl().contains("/storelocator/");
	            	Assert.assertEquals(true, status_Locator);
	            	Assert.assertEquals(getCommand().driver.getTitle(),dtvData.StoreFinder_PageTitle);
	            	log("Store Locator tab verified",LogType.STEP);
	            }
			}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Hero Navigation on DTV Page
	public Kohler_DTVPage verifyDTVHeroNav()
	{
		pageLoad();
		try
		{
			getCommand().waitFor(2);
			//Identify Navigation dots
			WebElement nav_section = getCommand().driver.findElement(By.xpath("//*[@id=\"fullPage-nav\"]/ul"));
    		List<WebElement> nav_links = nav_section.findElements(By.tagName("li"));
    		log("Clicking on each nav dot available in hero grid",LogType.STEP);
			int nav_index = 1;
    		
			//Verify navigation click and display
    		for (WebElement ele:nav_links)
    		{
    			if (ele.getAttribute("style") != "display: none;" && nav_index < nav_links.size())
    			{
    				ele.click();
    				getCommand().waitFor(5);
    				WebElement itemClass = getCommand().driver.findElement(By.xpath("//*[@id=\"fullPage-nav\"]/ul/li["+nav_index+"]/a"));
    				Assert.assertEquals("active", itemClass.getAttribute("class"));
    				log("Clicked on Nav dot:"+nav_index+" and verified",LogType.STEP);
    				nav_index++;
    			}
    		}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Global Navigation on DTV Page
	public Kohler_DTVPage verifyDTVGlobalNav()
	{
		pageLoad();
		try
		{
			getCommand().waitFor(2);
			//Verify global nav is visible on page load
			WebElement global_nav_DTV1 = getCommand().driver.findElement(By.xpath("//*[@id=\"fullPage-nav\"]/ul"));
			Assert.assertEquals(true, global_nav_DTV1.isDisplayed());
			log("Global navigation visible on page",LogType.STEP);
			
			//Scroll the page
			scrollDown();
			getCommand().waitFor(2);
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);			
			getCommand().waitFor(2);
			//Verify global nav is hidden on page scroll
			Assert.assertNotEquals(true, global_nav_DTV1.isDisplayed());
			log("Global Navigation not visible after scroll",LogType.STEP);
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Spa Experience on DTV Page
	public Kohler_DTVPage verifyDTVSpaExperience()
	{
		pageLoad();
		try
		{
			//Find scrubber element and scroll to it
			WebElement scrubber_element_DTV = getCommand().driver.findElement(By.xpath("//*[@id=\"detailsScrubber\"]/div[2]/div"));			
			scrollDown();
			getCommand().waitFor(2);
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", div_promo);
			js.executeScript("window.scrollBy(0,-250)", "");
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", scrubber_div_DTV);
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", scrubber_DTV);
			js.executeScript("window.scrollBy(0,-150)", "");
			getCommand().waitFor(2);
			//Verify auto play
			Assert.assertNotEquals("left: 0%;", scrubber_element_DTV.getAttribute("style"));
			log("Auto Play verified. Content playing and scrubber is at position: "+scrubber_element_DTV.getAttribute("style"),LogType.STEP);
			
			//Verify restart
			getCommand().click(scrubber_restart_DTV);
			Assert.assertNotEquals("left: 100%", scrubber_element_DTV.getAttribute("style"));
			log("Restart verified. Content restarted and scrubber is at position: "+scrubber_element_DTV.getAttribute("style"),LogType.STEP);
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Shower Experience on DTV Page
	public Kohler_DTVPage verifyDTVShowerExperience()
	{
		pageLoad();
		try
		{
			getCommand().waitFor(2);
			WebElement shower_dtv = getCommand().driver.findElement(By.xpath("//*[@id=\"dtv-shower-presets\"]/div[5]"));
			
			Actions actions = new Actions(getCommand().driver);
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
			
			
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", div_shower_bgs);
			
			
    		List<WebElement> shower_links_dtv = shower_dtv.findElements(By.className("dtv-shower-copy"));
    		ArrayList<String> list = new ArrayList<String>();
    		log("Identified shower experiences",LogType.STEP);
    		getCommand().waitFor(5);
    		
    		for (WebElement ele:shower_links_dtv)
    		{
    			String span_shower_dtv = "//*[@id=\""+ele.getAttribute("id") +"\"]" + "/span";
    			list.add(getCommand().driver.findElement(By.xpath(span_shower_dtv)).getText());
    			log("Shower Experience: "+getCommand().driver.findElement(By.xpath(span_shower_dtv)).getText()+" verified",LogType.SUBSTEP);
    			WebElement NextShower=getCommand().driver.findElement(By.xpath("//*[@id=\"dtv-shower-btnNext\"]"));
    			js.executeScript("arguments[0].click();", NextShower);
    			
    			//getCommand().click(btn_next_shower_DTV);
    			getCommand().waitFor(1);
    		}
    		CompareDataFromSameList(list);
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Helpers
	public Kohler_DTVPage scrollDown()
	{
		Actions actions = new Actions(getCommand().driver);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		return this;
	}
	
	public Kohler_DTVPage CompareDataFromSameList(List<String> list)
	{
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{
				if(list.get(i).equals(list.get(k)))
				{
					Assert.fail("Mismatch in data present in the list");
				}				      
			}	      
		}		
		return this;
	}
	
	public Kohler_DTVPage closeBrowser()
	{
		log("Close Browser",LogType.STEP);
		getCommand().closeCurrentWindow();
		return this;
	}
	
	public Kohler_DTVPage pageLoad()
	{
		try
		{
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			String test = js.executeScript("return document.readyState").toString();
			
			while (!test.equalsIgnoreCase("complete"))
			{
				getCommand().waitFor(1);
				test = js.executeScript("return document.readyState").toString();
			}
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_DTVPage openInTab(String xPath)
	{
		Actions builder = new Actions(getCommand().driver);
		WebElement element = getCommand().driver.findElement(By.xpath(xPath));
		Actions openInTab = builder.sendKeys(Keys.CONTROL).click(element);
		openInTab.perform();
		return this;
	}

}

