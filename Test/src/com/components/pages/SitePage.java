package com.components.pages;

import com.components.repository.SiteRepository;
import com.iwaf.framework.BasePage;



public class SitePage extends BasePage
{
protected SiteRepository repository;
	
	SitePage(SiteRepository repository)
	{
		this.repository=repository;
		
	}

		
	public Kohler_GeneralNavigation _GoToGeneralNavigation() 
	{
		return this.repository.generalNavigationPage();
	}
	
	public Hippo_Portables _GoToHippoPortables() 
	{
		return this.repository.hippoPortablePage();
	}
	
	public Kohler_DTVPage _gotoDTVPage()
	{
		return this.repository.dtvPage();	
	}
	
	public Kohler_SearchPage _GoToSearchPage()
	{
		return this.repository.searchResultPage();
	}
	
 }
	
