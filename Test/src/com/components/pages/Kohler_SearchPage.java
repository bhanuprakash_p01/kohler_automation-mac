package com.components.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.components.repository.SiteRepository;
import com.components.yaml.KohlerSearchData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;

public class Kohler_SearchPage extends SitePage{

	//Defining the Locators on the page
    public static final Target Search_box  = new Target("Search_box","//*[@id='nav-searchbox']",Target.XPATH);
    public static final Target Search_button  = new Target("Search_button","//*[@id='header__button--search-button-desktop']",Target.XPATH);
    public static final Target Inspiration  = new Target("Inspiration","//*[@id='search-hero-navigation']/a[2]",Target.XPATH);
    public static final Target Show_me_more = new Target("Show_me_more","Show me more",Target.LINKTEXT);
    public static final Target Resource = new Target("Resource","//*[@id='search-hero-navigation']/a[4]",Target.XPATH);
    public static final Target Read_the_article = new Target("Read_the_article","Read the article",Target.LINKTEXT);
    public static final Target Collection  = new Target("Collection","//*[@id='product-category-content']/div[1]/div[1]/h1",Target.XPATH);
    public static final Target Did_Mean_Two  = new Target("Did_Mean_Two","//*[@id='search-hero']/div/div[1]/h2[2]",Target.XPATH);
    public static final Target Article = new Target("Article", "//*[@id='product-categories']/ul/li[1]/a" , Target.XPATH);
    public static final Target Poplin_Products =new Target("Poplin_Products","//*[@id='search-results-panels']/div/a[1]/p[1]",Target.XPATH);
    public static final Target choreograph_Products =new Target ("choreograph_Products","//*[@id='search-results-panels']/div/a[1]/p[1]",Target.XPATH);
    public static final Target Link_parts = new Target("Link_parts","//button[@id= 'main-nav__button--parts']",Target.XPATH);	
    public static final Target Search_partsTerms =new Target("Search_partsTerms","searchTerms",Target.ID);
    public static final Target Link_partsMainMenu = new Target("Link_partsmainMenu","//*[@id='sub-nav-tab--parts']//following::a",Target.XPATH);
    public static final Target button_PartsSearchIcon = new Target("button_PartsSearchIcon","//*[@id='redesignSearchPartsForm']//following::button",Target.XPATH);
    public static final Target Kitchen_sinks = new Target("Kitchen_sinks",".//*[@id='section']/div[3]/div/div[1]/a/span",Target.XPATH);
    public static final Target Poplin_Product =new Target("Poplin_Product","//*[@id='search-results-panels']/div/a[1]",Target.XPATH);
    public static final Target Poplen_Product =new Target("Poplen_Product","//*[@id='search-results-panels']/div/a[1]",Target.XPATH);   
    public static final Target Poplen_Searchcount =new Target("Poplen_SearchCount","//*[@id='search-hero']/div/div[2]/h3",Target.XPATH);
    public static final Target Poplin_Searchcount =new Target("Poplin_SearchCoount","//*[@id='search-hero']/div/div[2]/h3",Target.XPATH);
    public static final Target Choreograph_Product =new Target ("choreograph_Product","//*[@id='search-results-panels']/div/a[1]",Target.XPATH);
    public static final Target Choreo_Product =new Target ("choreo_Product","//*[@id='search-results-panels']/div/a[1]",Target.XPATH);
    public static final Target Choreo_Searchcount =new Target("Choreo_SearchCount","//*[@id='search-hero']/div/div[2]/h3",Target.XPATH);
    public static final Target Choreograph_Searchcount =new Target("Choreograph_SearchCoount","//*[@id='search-hero']/div/div[2]/h3",Target.XPATH); 
    
  
	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities(); 
	JavascriptExecutor js =  (JavascriptExecutor)getCommand().driver;
    public Kohler_SearchPage(SiteRepository repository)
    {
    	super(repository);
    }

    //Navigating to Kohler Search Page
    public Kohler_SearchPage _GoToSearchPage()
    {
        log("Navigate to Kohler Search Page",LogType.STEP);
        return this;
    }
	
	// Verify Search functionality for Kitchen
	public Kohler_SearchPage VerifySearchFunctionalityKitchen()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				String PageTitle="Kitchen | KOHLER";
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchK = KohlerSearchData.fetch("SearchK");
				String SearchKFetched = SearchK.keyword;
				getCommand().sendKeys(Search_box, SearchKFetched);
				
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Kitchen Section Landing page ");
				log("Accessed to Kitchen Section Landing page",LogType.STEP);
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Toilets
	public Kohler_SearchPage VerifySearchFunctionalityToilets()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchT = KohlerSearchData.fetch("SearchT");
				String SearchTFetched = SearchT.keyword;
				getCommand().sendKeys(Search_box, SearchTFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				String PageTitle="Browse Kohler Toilets | KOHLER";
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Toilets category Landing page ");
				log("Accessed to Toilets Section Landing page",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Vanity
	public Kohler_SearchPage VerifySearchFunctionalityVanity()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchV = KohlerSearchData.fetch("SearchV");
				String SearchVFetched = SearchV.keyword;
				getCommand().sendKeys(Search_box, SearchVFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				String PageTitle="Bathroom Vanities |Bathroom | KOHLER";
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Vanity category Landing page ");
				log("Accessed to Vanity category Landing page", LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Faucet
	public Kohler_SearchPage VerifySearchFunctionalityFaucet()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchF = KohlerSearchData.fetch("SearchF");
				String SearchFFetched = SearchF.keyword;
				getCommand().sendKeys(Search_box, SearchFFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				String PageTitle="Kohler Faucets | KOHLER";
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Faucets Families Landing page ");
				log("Accessed to Faucets Families Landing page",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Products 14660-4-CP and K-73060-4-CP
	public Kohler_SearchPage VerifySearchFunctionalityProduct()
	{
		try {
				log("Verifying Search option in application with Product 14660-4-CP",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchP1 = KohlerSearchData.fetch("SearchP1");
				String SearchP1Fetched = SearchP1.keyword;
				getCommand().sendKeys(Search_box, SearchP1Fetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				String PageTitle="K-14660-4 | Loure Tall Single-Control Sink Faucet | KOHLER";
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Product 14660-4-CP page ");
				log("Accessed to Product 14660-4-CP page",LogType.STEP);
				
				log("Verifying Search option in application with product K-73060-4-CP",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchP2 = KohlerSearchData.fetch("SearchP2");
				String SearchP2Fetched = SearchP2.keyword;
				getCommand().sendKeys(Search_box, SearchP2Fetched);
				
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
			
				String PageTitle1="K-73060-4 | Composed Widespread Bathroom Sink Faucet | KOHLER";
				
				WebDriverWait wait1 = new WebDriverWait(getCommand().driver, 180);
				wait1.until(ExpectedConditions.titleContains(PageTitle1));
				
				String CurrentpageTitle1 = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle1, CurrentpageTitle1, "Not accessed to Product K-73060-4-CP page ");
				log("Accessed to Product K-73060-4-CP page ",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Arm
	public Kohler_SearchPage VerifySearchFunctionalityArm()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchAr = KohlerSearchData.fetch("SearchAr");
				String SearchArFetched = SearchAr.keyword;
				getCommand().sendKeys(Search_box, SearchArFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
				
				List<WebElement> SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='search-hero-navigation']/a"));
				int link_count = SubMenuLinks.size();
				
				String[] submenusText = {"PRODUCTS", "INSPIRATION", "PARTS" , "RESOURCES" , "TECHNICAL"};
 
				for (int i = 0; i < link_count; i++)
				{
					String link_text = SubMenuLinks.get(i).getText();
					getCommand().waitFor(5);
					
					Assert.assertTrue(link_text.contains(submenusText[i]), "The search keyword Arm not returned the sublinks ");
					
					if(link_text.contains(submenusText[i]))
					{
						log("Verify sub menu links: " +link_text ,LogType.STEP);
					}
				} 
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Inspiration tabs
	public Kohler_SearchPage VerifyFunctionalityInspiration()
	{
		String browserName=caps.getBrowserName();
		try {
			
			log("Verifying Search option in application",LogType.STEP);
			getCommand().isTargetVisible(Search_box);
			getCommand().clear(Search_box);
			
			KohlerSearchData SearchTop = KohlerSearchData.fetch("SearchTop");
			String SearchTopFetched = SearchTop.keyword;
			getCommand().sendKeys(Search_box, SearchTopFetched);
  	      	
			log("Click on search button",LogType.STEP);
  	      	getCommand().isTargetVisible(Search_button);
  	      	getCommand().click(Search_button);
			
  	      	WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
	     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
  	      	
			String Url="ideas.kohler.com";
			log("Click on Inspiration link",LogType.STEP);
			getCommand().click(Inspiration);
			getCommand().waitFor(5);
			WebElement LinkText = getCommand().driver.findElement(By.linkText("Show me more"));
			if(browserName.equals("safari"))
			{
				String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN);
				LinkText.sendKeys(selectLinkOpeninNewTab);

			}
			else
			{
				getCommand().click(Show_me_more);

			}
					
			ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			if(browserName.equals("safari"))
			{

					getCommand().driver.switchTo().window(tabs.get(0));  	
			}
			else
		    {
	    		getCommand().driver.switchTo().window(tabs.get(1));  	

		    }
			
		   WebDriverWait wait1 = new WebDriverWait(getCommand().driver,180);
	       wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='content col2x']/div/h1")));

			String CurrentUrl=getCommand().driver.getCurrentUrl();
			log("CurrentUrl" +Url,LogType.STEP);
			
			Assert.assertTrue(CurrentUrl.contains(Url), "The Inspiration tab not redirecting to ideas.kohler.com");
			log("The Inspiration tab redirects to ideas.kohler.com",LogType.STEP);
			
			getCommand().driver.close();
			getCommand().waitFor(5);
			if(browserName.equals("safari"))
			{

					getCommand().driver.switchTo().window(tabs.get(1));  	
			}
			else
		    {
	    		getCommand().driver.switchTo().window(tabs.get(0));  	

		    }
			getCommand().waitFor(5);
			
		}catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	// Verify Search functionality for Resource tabs
	public Kohler_SearchPage VerifyFunctionalityResource()
	{
		try {
			
			String Url="ideas.kohler.com";
			log("Click on Resource link",LogType.STEP);
			getCommand().click(Resource);
			
			getCommand().waitFor(5);
			getCommand().isTargetVisible(Read_the_article);
  	       	getCommand().click(Read_the_article);
  	       	
			getCommand().driver.getWindowHandle();
			String CurrentUrl=getCommand().driver.getCurrentUrl();
			
			Assert.assertTrue(CurrentUrl.contains(Url), "The Resource tab redirects to ideas.kohler.com");
			if(CurrentUrl.contains(Url)) {
				log("The results are shown from "+Url,LogType.STEP);
			}
			getCommand().driver.navigate().back();
			
		}catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	// Verify Search functionality for Memoirs
	public Kohler_SearchPage VerifySearchFunctionalityMemoirs()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchMem = KohlerSearchData.fetch("SearchMem");
				String SearchMemFetched = SearchMem.keyword;
				getCommand().sendKeys(Search_box, SearchMemFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='product-category-content']/div[1]/div[1]/h1")));
				
				getCommand().isTargetVisible(Collection);
				String collect_text=getCommand().getText(Collection);
				
				Assert.assertTrue(collect_text.contains("Collections"), "Collection is not displayed");
				log("Collection is displayed",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Moxie
	public Kohler_SearchPage VerifySearchFunctionalityMoxie()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchMox = KohlerSearchData.fetch("SearchMox");
				String SearchMoxFetched = SearchMox.keyword;
				getCommand().sendKeys(Search_box, SearchMoxFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.urlContains("article"));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				
				Assert.assertTrue(CurrentPageUrl.contains("article"), "Moxie Article is not displayed");
				log("Moxie Article is displayed",LogType.STEP);
				        		
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Two and results
	public Kohler_SearchPage VerifySearchFunctionalityTw()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchTw = KohlerSearchData.fetch("SearchTw");
				String SearchTwFetched = SearchTw.keyword;
				getCommand().sendKeys(Search_box, SearchTwFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[1]/h2[2]")));
			
				String search_for_two=getCommand().getText(Did_Mean_Two);
				Assert.assertTrue(search_for_two.contains("Did you mean two?"), "Did you mean two? is not displayed");
				log("Did you mean two? is displayed",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify typeahead with 3 letter i.e.Tou
	public Kohler_SearchPage VerifySearchFunctionalityTou()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchTou = KohlerSearchData.fetch("SearchTou");
				String SearchTouFetched = SearchTou.keyword;
				getCommand().sendKeys(Search_box, SearchTouFetched);
			
				getCommand().waitFor(5);
				
				List<WebElement> linkElements= getCommand().driver.findElements(By.xpath("//*[@id='search-suggestions--desktop']/div[1]/div"));
				String[] linkTexts= new String[linkElements.size()];		
				int i=0;
				
				for (WebElement e : linkElements) 
				{							
					linkTexts[i] = e.getText();		
					Assert.assertTrue(linkTexts[i].contains("Touchless"),"Typeahead with 3 letters results are not displayed");
					
					if(linkTexts[i].contains("Touchless"))
					log("Typeahead with 3 letters results are displayed",LogType.STEP);
					i++;		
		        }
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search term(Leed) and the page loads to external website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_Leed()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchL = KohlerSearchData.fetch("SearchL");
				String SearchLFetched = SearchL.keyword;
				getCommand().sendKeys(Search_box, SearchLFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.urlContains("www.us.kohler.com"));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				
				Assert.assertTrue(CurrentPageUrl.contains("Sustainability"), "The searched term 'leed' not opened in an external website");
				getCommand().waitFor(10);
				
				log("The searched term 'leed' opened in an external website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify Search term(Robern) and the page loads to external website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_Robern()
	{
		try {
			
			    getCommand().driver.navigate().back();
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchR = KohlerSearchData.fetch("SearchR");
				String SearchRFetched = SearchR.keyword;
				getCommand().sendKeys(Search_box, SearchRFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.urlContains("www.robern.com"));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				
				Assert.assertTrue(CurrentPageUrl.contains("robern"), "The searched term 'robern' not opened in an external website");
				getCommand().waitFor(10);
				
				log("The searched term 'robern' opened in an external website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search term(Privacy) and the page loads to external website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_Privacy()
	{
		try {
			
				getCommand().driver.navigate().back();
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchP = KohlerSearchData.fetch("SearchP");
				String SearchPFetched = SearchP.keyword;
				getCommand().sendKeys(Search_box, SearchPFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.urlContains("www.kohlercompany.com"));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				
				Assert.assertTrue(CurrentPageUrl.contains("privacy"), "The searched term 'privacy' not opened in an external website");
				
				getCommand().waitFor(10);
				
				log("The searched term 'privacy' opened in an external website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage()); 
		}
		return this;
	}
	
	// Verify Search term(Bath Tub) and the page loads to internal website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_BathTub()
	{
		try {
			
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchBT = KohlerSearchData.fetch("SearchBT");
				String SearchBTFetched = SearchBT.keyword;
				getCommand().sendKeys(Search_box, SearchBTFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='product-category-content']/div[1]/div[1]/h1")));
				
		     	String PageTitle="Bathing Bathtubs, Whirlpool, Bathing Products | Bathroom | KOHLER";
				String CurrentPageTitle=getCommand().getPageTitle();
		     	
		     	getCommand().waitFor(10);
				
				Assert.assertEquals(PageTitle, CurrentPageTitle, "The searched term 'bath tub' not opened in an internal website");
				log("The searched term 'Bath tub' opened in an internal website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify Search term(Night Light) and the page loads to internal website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_NightLight()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchNL = KohlerSearchData.fetch("SearchNL");
				String SearchNLFetched = SearchNL.keyword;
				getCommand().sendKeys(Search_box, SearchNLFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='article-page']/div[1]/div/div[1]/div/div/h1")));

		     	String PageTitle="Nightlight – Lighted toilet seats by Kohler";
				String CurrentPageTitle=getCommand().getPageTitle();

		     	getCommand().waitFor(10);
				Assert.assertEquals(PageTitle, CurrentPageTitle, "The searched term 'Night light' not opened in an internal website");
				log("The searched term 'Night light' opened in an internal website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify Search term(Pressure Balance) and the page loads to internal website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_PressureBalance()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().isTargetVisible(Search_box);
				getCommand().clear(Search_box);
				
				KohlerSearchData SearchPB = KohlerSearchData.fetch("SearchPB");
				String SearchPBFetched = SearchPB.keyword;
				getCommand().sendKeys(Search_box, SearchPBFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(Search_button);
				getCommand().click(Search_button);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='product-category-content']/div[1]/div[1]/h1")));
				
		     	String PageTitle="Pressure Balancing Valves & Trim | Showering |Bathroom | KOHLER";
				String CurrentPageTitle=getCommand().getPageTitle();
				
		     	getCommand().waitFor(10);
				Assert.assertEquals(PageTitle, CurrentPageTitle, "The searched term 'pressure balance' not opened in an internal website");
				log("The searched term 'pressure balance' opened in an internal website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
/* Vimala Scripts*/
	
	//  Verify article redirects are turned off by searching 'moxie'
	public Kohler_SearchPage VerifyArticlesTurnedOffbyParts()
	{
		try
		{
			log("Verifying Parts Landing page in application",LogType.STEP);
			getCommand().isTargetVisible(Link_parts);
			getCommand().click(Link_parts);
			
			log("Click on Parts menu",LogType.STEP);
			getCommand().isTargetVisible(Link_partsMainMenu);
			getCommand().click(Link_partsMainMenu);
			getCommand().waitForTargetPresent(Search_partsTerms);
			
			KohlerSearchData SearchMox = KohlerSearchData.fetch("SearchMox");
			String SearchMoxFetched = SearchMox.keyword;
			getCommand().sendKeys(Search_partsTerms, SearchMoxFetched);
			
			log("Click on search button",LogType.STEP);
			getCommand().isTargetVisible(button_PartsSearchIcon);
			getCommand().click(button_PartsSearchIcon);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
	     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
			
			String CurrentPageUrl= getCommand().driver.getCurrentUrl();
			
			Assert.assertTrue(!CurrentPageUrl.contains("article"),"Moxie is not an article page");
			
			log("Moxie is not an article page",LogType.STEP);
			
	    }
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
	    }
	    return this;
	}
	
	// Verify collections redirects are turned off by searching 'memoirs'
	public Kohler_SearchPage VerifyCollectionsTurnedOffbyParts()
	{ 
		try
		{
			log("Verifying Parts Landing page in application",LogType.STEP);
			getCommand().isTargetVisible(Link_parts);
			getCommand().click(Link_parts);
			
			log("Click on Parts menu",LogType.STEP);
			getCommand().isTargetVisible(Link_partsMainMenu);
			getCommand().click(Link_partsMainMenu);
			getCommand().waitForTargetPresent(Search_partsTerms);
			
			KohlerSearchData SearchMem = KohlerSearchData.fetch("SearchMem");
			String SearchMemFetched = SearchMem.keyword;
			getCommand().sendKeys(Search_partsTerms, SearchMemFetched);
			
			log("Click on search button",LogType.STEP);
			getCommand().isTargetVisible(button_PartsSearchIcon);
			getCommand().click(button_PartsSearchIcon);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
	     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
			
			String CurrentPageUrl= getCommand().driver.getCurrentUrl();
			Assert.assertTrue(!CurrentPageUrl.contains("collections"),"Memoir collections are not listed here");
			log("Memoir Collections page is not shown here",LogType.STEP);
			     		
	    }
		catch(Exception ex)
	    {
			Assert.fail(ex.getMessage());
	    }
	    	return this;
	}
	
	
	//Method to verify each term display the same results
    public Kohler_SearchPage VerifySearchFunctionalityTermsDisplaySameResults()
    {
    	try 
    	{
           log("Verifying Poplen Search results",LogType.STEP);
           getCommand().isTargetVisible(Search_box);
           getCommand().clear(Search_box);
           
           KohlerSearchData SearchPop = KohlerSearchData.fetch("SearchPop");
           String SearchPopFetched = SearchPop.keyword;
           getCommand().sendKeys(Search_box, SearchPopFetched);
           
           getCommand().isTargetVisible(Search_button);
           getCommand().click(Search_button);
           
           WebDriverWait wait1 = new WebDriverWait(getCommand().driver, 180);
           wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
    
           String ProductTitlePoplen=getCommand().getAttributeValue(Poplen_Product, "href");
           String PoplenCount=getCommand().getText(Poplen_Searchcount);
    
           getCommand().driver.navigate().back();
           getCommand().waitFor(2);
    
           log("Verifying Poplin Search results",LogType.STEP);
           getCommand().isTargetVisible(Search_box);
           getCommand().clear(Search_box);
           
           KohlerSearchData SearchPoplin = KohlerSearchData.fetch("SearchPoplin");
           String SearchPoplinFetched = SearchPoplin.keyword;
           getCommand().sendKeys(Search_box, SearchPoplinFetched);
           
           getCommand().isTargetVisible(Search_button);
           getCommand().click(Search_button);
           
           WebDriverWait wait2 = new WebDriverWait(getCommand().driver, 180);
           wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));        
    
           String ProductTitlePoplin=getCommand().getAttributeValue(Poplin_Product, "href");
          String PoplinCount=getCommand().getText(Poplen_Searchcount);
    
           if(ProductTitlePoplen.toLowerCase().contains("Poplin".toLowerCase()) && ProductTitlePoplin.toLowerCase().contains("Poplin".toLowerCase()))
           {
                 Assert.assertEquals(PoplenCount, PoplinCount, "Search results for Poplin and Poplen products are different");
                 log("Poplen/Poplin search results are similar",LogType.STEP);
           }                    
           else
           {
                 log("Poplin/Polin search results are dissimilar",LogType.ERROR_MESSAGE);
                 Assert.fail();
           }
           
           getCommand().driver.navigate().back();
           log("Verifying Chreo Search results",LogType.STEP);
           getCommand().isTargetVisible(Search_box);
           getCommand().clear(Search_box);
           
           KohlerSearchData SearchChoreo = KohlerSearchData.fetch("SearchChoreo");
           String SearchChoreoFetched = SearchChoreo.keyword;
           getCommand().sendKeys(Search_box, SearchChoreoFetched);
           
           getCommand().isTargetVisible(Search_button);
           getCommand().click(Search_button);
           
           WebDriverWait wait3 = new WebDriverWait(getCommand().driver, 180);
           wait3.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
    
           String ProductTitleChoreo=getCommand().getAttributeValue(Choreo_Product, "href");
           String ChoreoCount=getCommand().getText(Choreo_Searchcount);
    
           getCommand().driver.navigate().back();
    
           log("Verifying Choreograph search results",LogType.STEP);
           getCommand().isTargetVisible(Search_box);
           getCommand().clear(Search_box);
           
           KohlerSearchData SearchChoreograph = KohlerSearchData.fetch("SearchChoreograph");
           String SearchChoreographFetched = SearchChoreograph.keyword;
           getCommand().sendKeys(Search_box, SearchChoreographFetched);
           
           getCommand().isTargetVisible(Search_button);
           getCommand().click(Search_button);
           
           WebDriverWait wait4 = new WebDriverWait(getCommand().driver, 180);
           wait4.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));        
    
           String ProductTitleChoreograph=getCommand().getAttributeValue(Choreograph_Product, "href");
           String ChoreographCount=getCommand().getText(Choreograph_Searchcount);
    
           if(ProductTitleChoreo.toLowerCase().contains("Choreograph".toLowerCase()) && ProductTitleChoreograph.toLowerCase().contains("Choreograph".toLowerCase()))
           {
                 Assert.assertEquals(ChoreoCount, ChoreographCount, "Search results for choreo and Choreograph products are different");
                 log("Choreo/Choreograph search results are similar",LogType.STEP);
           }
           
           else
           {
                 log("Choreo/Choreograph search results are dissimilar",LogType.ERROR_MESSAGE);
                 Assert.fail();
           }
           
    }
    catch(Exception ex)
    {
           Assert.fail(ex.getMessage());
    }
    return this;
           
    }             
}
