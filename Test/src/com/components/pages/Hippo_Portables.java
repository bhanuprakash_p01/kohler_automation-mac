/**
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 *																																		   	*
 * 2011-2012 Infosys Limited, Banglore, India. All Rights Reserved																			*
 * Version: 2.0																																*
 * 																																			*
 * Except for any free or open source software components embedded in this Infosys proprietary software program ("Program"),				*
 * this Program is protected by copyright laws, international treaties and other pending or existing intellectual property rights in India, *
 * the United States and other countries. Except as expressly permitted, any unautorized reproduction, storage, transmission 				*
 * in any form or by any means (including without limitation electronic, mechanical, printing, photocopying, recording or otherwise), 		*
 * or any distribution of this Program, or any portion of it, may result in severe civil and criminal penalties, 							*
 * and will be prosecuted to the maximum extent possible under the law 																		*
 *																																			*
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 **/
package com.components.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;


import com.components.repository.SiteRepository;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;


public class Hippo_Portables extends SitePage
{

	/* Defining the locators on the Page */ 
	
	//public static final Target LOGO = new Target("target-logo","hplogo",Target.ID);
	//public static final Target btn_SignIn = new Target("btn_SignIn","//*[@id='gb_70']",Target.XPATH);
	public static final Target Link_Portable_jobSite = new Target("Link_Portable_jobSite","//*[@id='main-nav']//following::a[text()='Job Site']",Target.XPATH);	
	public static final Target Link_Portable_HomePlay = new Target("Link_Portable_HomePlay","//*[@id='main-nav']//following::a[text()='Home & Play']",Target.XPATH);	
	public static final Target Link_Portable_CustomKits = new Target("Link_Portable_CustomKits","//*[@id='main-nav']//following::li[4]/a",Target.XPATH);	
	public static final Target Link_Portable_generator101 = new Target("Link_Portable_generator101","//*[@id='subnav-jobsite']//following::a[text()='Generators 101']",Target.XPATH);	
	public static final Target Link_Portable_generator = new Target("Link_Portable_generator","//*[@id='subnav-jobsite']//following::li[2]/a",Target.XPATH);	
	public static final Target Link_OwnerManual = new Target("Link_OwnerManual","//*[@id='subnav-homeplay']//following::li[6]/a",Target.XPATH);	
	
	public static final Target Article_Portable_PerfectGen = new Target("Article_Portable_PerfectGen","//div[@class='column-2 cms-block container ']",Target.XPATH);	
	public static final Target Article_Portable_PerfectGenheader = new Target("Article_Portable_PerfectGenheader","//div[@class='column-2 cms-block container ']/div[1]//following::h1[1]",Target.XPATH);	
	
	public static final Target Article_Portable_PortableGenerator = new Target("Article_Portable_PortableGenerator","//div[@class='icon-tabs cms-block ']/div[@class='container']",Target.XPATH);	
	public static final Target Article_Portable_PortableGeneratorheader = new Target("Article_Portable_PortableGeneratorheader","//div[@class='icon-tabs cms-block ']/div[@class='container']/h1",Target.XPATH);	
	
	public static final Target Article_Portable_UseitProperly = new Target("Article_Portable_UseitProperly","//div[@class='hst-container']",Target.XPATH);	
	public static final Target Article_Portable_UseitProperlyheader = new Target("Article_Portable_UseitProperlyheader","//div[@class='hst-container']//following::div[@class='copy-wrap pull-left']/h1",Target.XPATH);	
	
	public static final Target Article_Portable_GiveUsAShout = new Target("Article_Portable_GiveUsAShout","//div[@class='column-2 banner-action cms-block container']",Target.XPATH);	
	public static final Target Article_Portable_GiveUsAShoutheader = new Target("Article_Portable_GiveUsAShoutheader","//div[@class='column-2 banner-action cms-block container']//following::div[@class='banner box box-large pull-left']/h1",Target.XPATH);
	
	public static final Target Article_Portable_HomePlayPortableGenerator = new Target("Article_Portable_HomePlayPortableGenerator","//div[@id='asset-1365055158359']",Target.XPATH);	
	public static final Target Article_Portable_HomePlayPortableGeneratorheader = new Target("Article_Portable_HomePlayPortableGeneratorheader","//div[@id='asset-1365055158359']//following::h1",Target.XPATH);	
	
	public static final Target Article_Portable_HomePlayPortableGeneratorrated = new Target("Article_Portable_HomePlayPortableGeneratorrated","//div[@id='asset-1365055281271']",Target.XPATH);	
	public static final Target Article_Portable_HomePlayPortableGeneratorratedheader = new Target("Article_Portable_HomePlayPortableGeneratorratedheader","//div[@id='asset-1365055281271']//following::div[@class='container']/h1",Target.XPATH);	
	
	public static final Target Slider_Portables_KW_Range = new Target("Slider_Portables_KW_Range","//*[@id='kwSlider']/a[1]/span",Target.XPATH);	
	public static final Target Slider_Portables_MSRP_Range = new Target("Slider_Portables_MSRP_Range","//*[@id='msrpSlider']/a[1]/span",Target.XPATH);	
	public static final Target link_Portables_SortBy = new Target("link_Portables_SortBy","//div[@class='sort-by']//following::div[@class='ffSelect']/a",Target.XPATH);	
	public static final Target link_Portables_view = new Target("link_Portables_view","//div[@class='per-page']//following::div[@class='ffSelect']/a",Target.XPATH);	
	public static final Target Text_Portables_PageCount = new Target("Text_Portables_PageCount","//div[@id='paging-top-container']//following::div[@class='paging']/div[1]/span[@class='total-count']",Target.XPATH);	
	public static final Target Text_FindADealer_Title = new Target("Text_FindADealer_Title","//div[@class='page-title ']/h1",Target.XPATH);	
	public static final Target Text_GeneratorsTitle = new Target("Text_GeneratorsTitle","//div[@class='page-title']/h1",Target.XPATH);	
	public static final Target Text_GeneratorViewresults = new Target("Text_GeneratorViewresults","//*[@id='get-quote']",Target.XPATH);	
	
	
	
	public Hippo_Portables(SiteRepository repository)
	{
		super(repository);
	}

	/* Functions on the Page are defined below */
	
	/*public Hippo_Portables atHomePage()
	{
		log("Launched Hippo Site",LogType.STEP);
		//getCommand().captureScreenshot("C:\\Users\\Arvind01\\Desktop\\Add To Cart\\HomePage.png");
		return this;
		
	}*/

	// Verify the Article Page from generator 101
	public Hippo_Portables VerifyArticlePage()
	{
		try {
				log("Verifying Article Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_jobSite);
				getCommand().click(Link_Portable_jobSite);
				
				if(getCommand().getPageUrl().contains("job-site"))
				{
					log("Navigated to Job site Page",LogType.STEP);
				}
				else
				{
					log("Was unable to navigated to Job site Page" + getCommand().getPageUrl() ,LogType.STEP);
					Assert.fail("Was unable to navigated to Job site Page");
				}
				
				if(getCommand().isSelected(Link_Portable_generator101))
					log("In Generator 101 Page",LogType.STEP);
					Assert.assertTrue(true, "Generator 101 is Selected by default");
				
				if(getCommand().isTargetVisible(Article_Portable_PerfectGen))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_PerfectGenheader);
					Assert.assertEquals(Actualheader, "Find your perfect generator.", "Header miss matches for Article_Portable_PerfectGen");
				}
				
				if(getCommand().isTargetVisible(Article_Portable_PortableGenerator))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_PortableGeneratorheader);
					Assert.assertEquals(Actualheader, "How is a\nportable generator rated?", "Header miss matches for Article_Portable_PortableGenerator");
				}
				
				if(getCommand().isTargetVisible(Article_Portable_UseitProperly))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_UseitProperlyheader);
					Assert.assertEquals(Actualheader, "How to use it properly", "Header miss matches for Article_Portable_UseitProperly");
				}
				
					
				if(getCommand().isTargetVisible(Article_Portable_GiveUsAShout))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_GiveUsAShoutheader);
					Assert.assertEquals(Actualheader, "Give Us a Shout", "Header miss matches for Article_Portable_GiveUsAShout");
				}
				
				log("verify the article in Home & Play Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_HomePlay);
				getCommand().click(Link_Portable_HomePlay);
				
				if(getCommand().getPageUrl().contains("home-play"))
				{
					log("Navigated to Job site Page",LogType.STEP);
				}
				else
				{
					log("Was unable to navigated to Home & Play Page" + getCommand().getPageUrl() ,LogType.STEP);
					Assert.fail("Was unable to navigated to Home & Play Page");
				}
				
				if(getCommand().isSelected(Link_Portable_generator101))
					log("In Generator 101 Page",LogType.STEP);
					Assert.assertTrue(true, "Generator 101 is Selected by default");
				
				if(getCommand().isTargetVisible(Article_Portable_PerfectGen))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_PerfectGenheader);
					Assert.assertEquals(Actualheader, "Find your perfect generator.", "Header miss matches for Article_Portable_PerfectGen");
				}
				
				if(getCommand().isTargetVisible(Article_Portable_HomePlayPortableGeneratorrated))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_HomePlayPortableGeneratorratedheader);
					Assert.assertEquals(Actualheader, "How is a\nportable generator rated?", "Header miss matches for Article_Portable_PortableGenerator");
				}
				
				if(getCommand().isTargetVisible(Article_Portable_HomePlayPortableGenerator))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_HomePlayPortableGeneratorheader);
					Assert.assertEquals(Actualheader, "What is a\nportable generator?", "Header miss matches for Article_Portable_PortableGenerator");
				}
				
				if(getCommand().isTargetVisible(Article_Portable_UseitProperly))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_UseitProperlyheader);
					Assert.assertEquals(Actualheader, "How to use it properly", "Header miss matches for Article_Portable_UseitProperly");
				}
				
					
				if(getCommand().isTargetVisible(Article_Portable_GiveUsAShout))
				{
					log("In Generator 101 Page",LogType.STEP);
					String Actualheader = getCommand().getText(Article_Portable_GiveUsAShoutheader);
					Assert.assertEquals(Actualheader, "Give Us a Shout", "Header miss matches for Article_Portable_GiveUsAShout");
				}
				
				
						
				
				
				
		}catch(Exception ex)
		{
			ex.getMessage();
		}
		

		return this;
	}
	
	
	// Verify the Article Page from generator 101
	public Hippo_Portables VerifyOwnerManualsPage()
		{
			try {
					
					log("verify the article in Home & Play Page",LogType.STEP);
					getCommand().isTargetVisible(Link_Portable_HomePlay);
					getCommand().click(Link_Portable_HomePlay);
				
					if(getCommand().getPageUrl().contains("home-play"))
					{
						log("Navigated to Job site Page",LogType.STEP);
					}
					else
					{
						log("Was unable to navigated to Home & Play Page" + getCommand().getPageUrl() ,LogType.STEP);
						Assert.fail("Was unable to navigated to Home & Play Page");
					}
					
					log("Navigated to Owners Manual Page",LogType.STEP);
					getCommand().isTargetVisible(Link_OwnerManual);
					getCommand().click(Link_OwnerManual);
				
					if(getCommand().getPageUrl().contains("owners-manuals"))
					{
						log("Navigated to owners manuals Page",LogType.STEP);
					}
					else
					{
						log("Was unable to navigated to owners manuals Page" + getCommand().getPageUrl() ,LogType.STEP);
						Assert.fail("Was unable to navigated to owners manuals Page");
					}
					
					
					List<WebElement> Pdfs = getCommand().driver.findElements(By.xpath("//*[@id='product-matrix']/div/ul/li"));
					int count = Pdfs.size();
					
					for (int i = 1; i <= count; i++) 
					{
						
						String PdfText = getCommand().driver.findElement(By.xpath("//*[@id='product-matrix']/div/ul/li["+i+"]//following::h3[1]")).getText();
						log("PDF with PDF text is :"+ PdfText ,LogType.STEP);
						WebElement DwnLink = getCommand().driver.findElement(By.xpath("//*[@id='product-matrix']/div/ul/li["+i+"]//following::p[3]/a"));
						
						
						int Win_size = getCommand().driver.getWindowHandles().size();
						log("Verify the size of open windows currently: "+ Win_size,LogType.STEP);
						
						log("click on each link and verify in new tab",LogType.STEP);
						DwnLink.click();
					
						getCommand().waitFor(10);
						
						ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
						
						getCommand().driver.switchTo().window(tabs2.get(1));
						Win_size = getCommand().driver.getWindowHandles().size();
					    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
					    
						if(getCommand().driver.getCurrentUrl().contains("pdf"))
						{
							log("Page is navigated to PDF page with print option" ,LogType.STEP);
						}
						else
						{
							log("Page is not navigated to PDF page " ,LogType.STEP);
							Assert.fail("Page is not navigated to PDF page");
						}
						
						
						getCommand().driver.close();
						log("Close the tab and switch back to parent window",LogType.STEP);
						getCommand().driver.switchTo().window(tabs2.get(0));
						
						
					}
					
					
				
			}catch(Exception ex)
			{
				ex.getMessage();
			}
			

			return this;
			
		}
			
	// Verify breadcrumbs in product category Page
	public Hippo_Portables VerifyBreadcrumbs()
	{
		try {
				log("Verifying Job site Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_jobSite);
				getCommand().click(Link_Portable_jobSite);
				
				if(getCommand().getPageTitle().contains("job-site"))
				Assert.assertTrue(true, "Successfully in job site Page");
			
				log("Verifying Generator Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
				
				if(getCommand().getPageTitle().contains("generators"))
				Assert.assertTrue(true, "Successfully in generators Page");
			
				log("Verify the breadcrumb navigation and get the size",LogType.STEP);
				List<WebElement> breadcrumbs =  getCommand().driver.findElements(By.xpath("//div[@class='breadcrumbs container clearfix']/ul/li"));
				int count = breadcrumbs.size();
				
				log("Verify breadcrumbs last text",LogType.STEP);
				String breadcrumbs_Page = getCommand().driver.findElement(By.xpath("//div[@class='breadcrumbs container clearfix']/ul/li["+count+"]")).getText();
				
				log("retieve the page url",LogType.STEP);
				String PageURL = getCommand().getPageUrl();
				String LastString = PageURL.substring(PageURL.lastIndexOf("/"), PageURL.length());
				String Page_LstStr = LastString.replace("/", "");
			
				if(Page_LstStr.equalsIgnoreCase(breadcrumbs_Page))
				{
					log("breadcrumbs last text is same as page Url last text",LogType.STEP);
					Assert.assertTrue(true, "Breadcrumbs shows correct page");
				}
				else
				{	log("breadcrumbs last text is not same as page Url last text",LogType.STEP);
					Assert.fail(Page_LstStr+"is not matching with "+breadcrumbs_Page+"description"); 
				}
			

		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return this;
	}
	
	// verify Slider filter in product category Page
	public Hippo_Portables verifyProductCategorySlider()
	{
		ArrayList<String> KW_values = new ArrayList<String>();
		ArrayList<String> MSRP_values = new ArrayList<String>();
		try {
				log("Verifying Job site Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_jobSite);
				getCommand().click(Link_Portable_jobSite);
				
				if(getCommand().getPageTitle().contains("job-site"))
					Assert.assertTrue(true, "Successfully in job site Page");
		
				log("Verifying Generator Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
			
				if(getCommand().getPageTitle().contains("generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				
				
				log("Verifying that slider is visble in generators Page ",LogType.STEP);
				WebElement KW_lefthand = getCommand().driver.findElement(By.xpath("//*[@id='kwSlider']/a[1]"));
				Actions action = new Actions(getCommand().driver);
				
				log("drag the slider to certain range",LogType.STEP);
				action.dragAndDropBy(KW_lefthand, 100, 380).release().build().perform();
				KW_lefthand.click();
	        
				getCommand().waitFor(5);
				getCommand().isTargetVisible(Slider_Portables_KW_Range);
				String sliderRange = getCommand().getText(Slider_Portables_KW_Range);
				log("Retrieve the slider min range by draging the slider and get the range value: " + sliderRange,LogType.STEP);
	        
				log("Verify that Product category grid is loaded based on slider range",LogType.STEP);
				List<WebElement> KW_products = getCommand().driver.findElements(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li"));
	        
				for (int i = 1; i <= KW_products.size(); i++)
				{
					WebElement product_MaxPower = getCommand().driver.findElement(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']/div[@class='product-specs']/div/ul/li"));
					log("Get the Max power text of product:" + product_MaxPower.getText(),LogType.STEP);
					KW_values.add(product_MaxPower.getText());
				}
	        
				Hippo_Portables.verifyStringRange(sliderRange, KW_values);
				
				log("Verifying that slider is visble in generators Page ",LogType.STEP);
				WebElement MSRP_lefthand = getCommand().driver.findElement(By.xpath("//*[@id='msrpSlider']/a[1]"));
				action = new Actions(getCommand().driver);
				
				log("drag the slider to certain range",LogType.STEP);
				action.dragAndDropBy(MSRP_lefthand, 100, 1047).release().build().perform();
				MSRP_lefthand.click();
	        
				getCommand().waitFor(5);
				getCommand().isTargetVisible(Slider_Portables_MSRP_Range);
				sliderRange = getCommand().getText(Slider_Portables_MSRP_Range);
				log("Retrieve the slider min range by draging the slider and get the range value: " + sliderRange,LogType.STEP);
	        
				log("Verify that Product category grid is loaded based on slider range",LogType.STEP);
				List<WebElement> MSRP_products = getCommand().driver.findElements(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li"));
	        
				for (int i = 1; i <= MSRP_products.size(); i++)
				{
					WebElement product_MaxPower = getCommand().driver.findElement(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']/div[@class='product-specs']/div/ul/li"));
					log("Get the Max power text of product:" + product_MaxPower.getText(),LogType.STEP);
					MSRP_values.add(product_MaxPower.getText());
				}
	        
	          	if(MSRP_products.size() < KW_products.size() )	
	          	{
	          		log("Product category page is updated successfully",LogType.STEP);
	          		Assert.assertTrue(true, "Product category page is updated successfully");
	          	}
	          	else
	          		Assert.fail("Product category page is not updated");
				
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return this;
	}
	
	// Verify Sorting filter in product category Page
	public Hippo_Portables verifyProductCategorySortingFilter()
	{
		try {	
				ArrayList<String> values = new ArrayList<String>();
				log("Verifying Job site Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_jobSite);
				getCommand().click(Link_Portable_jobSite);
			
				if(getCommand().getPageTitle().contains("job-site"))
					Assert.assertTrue(true, "Successfully in job site Page");
	
				log("Verifying Generator Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
		
				if(getCommand().getPageTitle().contains("generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
			
				log("identify sort by element and click on it",LogType.STEP);
				getCommand().isTargetVisible(link_Portables_SortBy);
				getCommand().click(link_Portables_SortBy);
								
				getCommand().waitFor(5);
				log("Verifying the list of element in sort by drop down",LogType.STEP);
				List<WebElement> drpElement = getCommand().driver.findElements(By.xpath("//div[@class='sort-by']//following::div[@class='ffSelectMenuWrapper']/div[@class='ffSelectMenuMidBG']/div[1]/ul[1]/li"));
				
				for(WebElement val : drpElement)
				{
					String valueIs = val.getText();
					if(valueIs.equals("Name"))
					{
						val.click();
						getCommand().waitFor(5);
						break;
						
					}
				}
				
				getCommand().waitFor(5);
				log("Verifying the updated products after sort in Product category",LogType.STEP);
				List<WebElement> products = getCommand().driver.findElements(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li"));
		        
				log("Verifying the max power after sort in Product category",LogType.STEP);
		        for (int i = 1; i <= products.size(); i++)
		        {
		        	WebElement product_MaxPower = getCommand().driver.findElement(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-header clearfix']/h2/div/a"));
		            String Name = product_MaxPower.getText();
		            values.add(Name);
				}
		        
		        
		        ArrayList<String> actualValueIs = new ArrayList<String>();
		        actualValueIs.addAll(values);
		        Collections.sort(actualValueIs);
		        
		        // compare the actual and expected Products based on sorting used
		        Hippo_Portables.VerifySortByNameIs(values,actualValueIs);

				
				
				
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return this;
	}
	
	// verify Pagination in Generators Page
	public Hippo_Portables verifyProductCategoryPagination()
	{
		try {
				
				log("Verifying Job site Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_jobSite);
				getCommand().click(Link_Portable_jobSite);
		
				if(getCommand().getPageTitle().contains("job-site"))
					Assert.assertTrue(true, "Successfully in job site Page");

				log("Verifying Generator Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
	
				
				if(getCommand().getPageTitle().contains("generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				
				log("Verifying View Drop down and click on it",LogType.STEP);
				getCommand().isTargetVisible(link_Portables_view);
				getCommand().click(link_Portables_view);
				
				getCommand().waitFor(5);
				List<WebElement> drpElement = getCommand().driver.findElements(By.xpath("//div[@class='per-page']//following::div[@class='ffSelectMenuWrapper']/div[@class='ffSelectMenuMidBG']/div[1]/ul[1]/li"));
				String drpdownminVal = drpElement.get(0).getText();
				
				for(WebElement val : drpElement)
				{
					String valueIs = val.getText();
					if(valueIs.equals("18"))
					{						
						val.click();
						getCommand().waitFor(5);
						break;
						
					}
				}

				log("Verifying the total page count",LogType.STEP);
				getCommand().isTargetVisible(Text_Portables_PageCount);
				String InttotalCount= getCommand().getText(Text_Portables_PageCount);
				
				log("Verifying total page count with dropdown count",LogType.STEP);
				if(Integer.parseInt(InttotalCount) <= Integer.parseInt(drpdownminVal))
				{

					WebElement previous = getCommand().driver.findElement(By.xpath("//div[@class='page-arrows']/div[@class='prev']/a"));
					
					WebElement next = getCommand().driver.findElement(By.xpath("//div[@class='page-arrows']/div[@class='next']/a"));
					
					boolean status = !previous.isDisplayed() && !next.isDisplayed();
					System.out.println(status);
				}
				else
				{
					WebElement previous = getCommand().driver.findElement(By.xpath("//div[@class='page-arrows']/div[@class='prev']/a"));
					
					WebElement next = getCommand().driver.findElement(By.xpath("//div[@class='page-arrows']/div[@class='next']/a"));
					
					boolean status = previous.isDisplayed() && next.isDisplayed();
					System.out.println(status);
					
					next.click();
					
				}

				
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		
		return this;
	}

	// Verify product modules 
	public Hippo_Portables verifyProductCategoryProductModules()
	{
		try {
				log("Verifying Job site Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_jobSite);
				getCommand().click(Link_Portable_jobSite);
	
				if(getCommand().getPageTitle().contains("job-site"))
					Assert.assertTrue(true, "Successfully in job site Page");

				log("Verifying Generator Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);

				if(getCommand().getPageTitle().contains("generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				
				getCommand().waitFor(5);
				List<WebElement> Products =  getCommand().driver.findElements(By.xpath("//div[@class='products-wrap']/ul/li"));
				int count = Products.size();
				
				for (int i = 1; i <= count; i++)
				{
					
					String ProductTitle = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div/h2/div/a")).getText();
					log("Verifying Product Title in Product category Page:" + ProductTitle,LogType.STEP);
					String ProductPrice = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div/div/div[@class='price']")).getText();
					log("Verifying Product Price in Product category Page:" + ProductPrice,LogType.STEP);
					boolean image_Product =  getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div[2]//following::img")).isDisplayed();
					log("Verifying image_Product is available in Product category Page:" + image_Product,LogType.STEP);
					String MaxPower = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']//following::div[@class='resources']/ul/li[1]")).getText();
					log("Verifying MaxPower is available in Product category Page:" + MaxPower,LogType.STEP);
					String ContinuousPower = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']//following::div[@class='resources']/ul/li[2]")).getText();
					log("Verifying ContinuousPower is available in Product category Page:" + ContinuousPower,LogType.STEP);
					String FuelType = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']//following::div[@class='resources']/ul/li[3]")).getText();
					log("Verifying FuelType is available in Product category Page:" + FuelType,LogType.STEP);
					
					
				}
				
				log("Verifying Find a dealer link and click on it",LogType.STEP);
				getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li[1]/div[@class='product-info clearfix']//following::div[@class='resources']/div[@class='view-detail'][1]/a")).click();
				
				log("Verifying that page is navigated to repsective page",LogType.STEP);
				getCommand().waitForTargetVisible(Text_FindADealer_Title, 10);
				if(getCommand().getPageTitle().contains("Find a Dealer"))
					Assert.assertTrue(true, "Successfully in Find a dealer Page");
				
				log("Verifying that user is navigated to generator page again",LogType.STEP);
				getCommand().driver.navigate().to("https://preprod.kohlerpower.kohler.com/en/powerequipment/job-site/products/generators");
				
				log("Verifying Page title in generator page",LogType.STEP);
				getCommand().waitForTargetVisible(Text_GeneratorsTitle, 10);
				if(getCommand().getPageTitle().contains("Job Site - Generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				
				log("Verifying More details link and click on it",LogType.STEP);
				getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li[1]/div[@class='product-info clearfix']//following::div[@class='resources']/div[@class='view-detail'][2]/a")).click();
				
				log("Verifying that page is navigated to repsective page",LogType.STEP);
				getCommand().waitForTargetVisible(Text_GeneratorViewresults, 10);
				if(getCommand().isTargetPresent(Text_GeneratorViewresults))
					Assert.assertTrue(true, "Successfully in results Page");
				
				log("Verifying that user is navigated to generator page again",LogType.STEP);
				getCommand().driver.navigate().to("https://preprod.kohlerpower.kohler.com/en/powerequipment/job-site/products/generators");
				
				log("Verifying that page is navigated to repsective page",LogType.STEP);
				getCommand().waitForTargetVisible(Text_GeneratorsTitle, 10);
				if(getCommand().getPageTitle().contains("Job Site - Generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return this;
	}
	
	// Verify Custom Kits page
	public Hippo_Portables VerifyCustomKits()
	{
		try {
				
				log("Verifying custom kits Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_CustomKits);
				getCommand().click(Link_Portable_CustomKits);

				if(getCommand().getPageTitle().contains("custom-kits"))
					Assert.assertTrue(true, "Successfully in Custom Kits Page");
			
				
				List<WebElement> thumbnails = getCommand().driver.findElements(By.xpath("//*[@id='asset-1365055281271']//following::dl/dt"));
				List<WebElement> Mainimages = getCommand().driver.findElements(By.xpath("//*[@class='content-image wrapper']//following-sibling::img"));
				int count1 = Mainimages.size();
				int count = thumbnails.size();
				if(count==count1)
				{
					Assert.assertEquals(count, count1 ,"Both the images are of same size");
					
					for (int i = 1; i <=count; i++)
					{
						WebElement thumbnailIcon = getCommand().driver.findElement(By.xpath("//*[@id='asset-1365055281271']//following::dl/dt["+i+"]/div"));
						System.out.println(thumbnailIcon.getText());
						thumbnailIcon.click();
						
						String thumbnail = getCommand().driver.findElement(By.xpath("//*[@id='asset-1365055281271']//following::dl/dt["+i+"]")).getAttribute("class");
						String mainimage = getCommand().driver.findElement(By.xpath("//*[@class='content-image wrapper']//following-sibling::img["+i+"]")).getAttribute("class");
							if(thumbnail.contains("active")==mainimage.contains("active"))
							{
								Assert.assertTrue(true, "Thumbnail image is displayed as main Image");
							}

					}
					
					getCommand().driver.findElement(By.xpath("//*[@id='asset-1365055281271']//following-sibling::a[@class='btn']")).click();
					
					if(getCommand().driver.getTitle().contains("products"))
						{
						Assert.assertTrue(true, "Successfully in Products Category Page");
						}
					
				}

			
		}catch(Exception ex)
		{
			ex.getMessage();
		}
		
		
		return this;
	}
	
	
	
	
	
	
	/*------------------------Hippo Portables common methods-----------------------------------*/
	
	
	// Compare min power range and product in product category 
		public static boolean verifyStringRange(String value, List<String> values)
		{
			boolean status = false;
			
			// Get the min power range from the slider and split to get the exact value
			String[] rangeVal = value.split(" ");
			String MinRangeVal =rangeVal[0];
			System.out.println(MinRangeVal);
			
			// loop the list to get the product power range and iterate through each to get the value
			for (int i = 0; i < values.size(); i++)
	        {	
	        	String[] val = values.get(i).split("\n");
	        	String val1 =val[1].toString();
	        	System.out.println(val1);
	        	
	        	String[] finalVal = val1.split(" ");
	        	String finalValueIs = finalVal[0];
	        	
	        	System.out.println(finalValueIs);
	        	
	        	// compare slider range with product power range and return the value
	        	int resultsIs = finalValueIs.compareTo(MinRangeVal);
	        	if(resultsIs == 0 || resultsIs >0)
	        		status = true;
				
			}			
			
			return status;
		}

		// verify that product are displayed in alphabetical order when sorted by NAME
		public static boolean VerifySortByNameIs(List<String> ExpectedVal ,List<String> ActualVal)
		{
			boolean status = false;
			if(ExpectedVal.size() ==  ActualVal.size())
			{
				if(ExpectedVal.equals(ActualVal))
					status = true;
			}
							
					
			return status;
		}
	
		public void FluentWait(Target ele)
		{
			
			// Waiting 30 seconds for an element to be present on the page, checking
			// for its presence once every 5 seconds.
			Wait<WebDriver> wait = new FluentWait<WebDriver>(getCommand().driver)
			    .withTimeout(80, TimeUnit.SECONDS)
			    .pollingEvery(5, TimeUnit.SECONDS)
			    .ignoring(NoSuchElementException.class);

			getCommand().isTargetVisible(ele);
		}

}