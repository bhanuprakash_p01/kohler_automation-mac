package com.components.yaml;

import com.iwaf.framework.BasePage;

public class LatamxSearchData  {
	
	public String keyword;
	//public String SearchElement;
	//public String itemCode;
	
	public static LatamxSearchData fetch(String key){
	BasePage pageObj = new BasePage();
	LatamxSearchData obj = pageObj.getCommand().loadYaml(key, "data-pool/LatamxSearch.yaml");
		return obj;
	}
}
